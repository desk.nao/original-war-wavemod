// export ThreatLevel ; var ChasisSelection , EngineSelection , WeaponSelection , MakeMe ; every 0 0$15 trigger IsOK ( AmDepot ) and IsOK ( AmFac ) and IsIdle ( AmFac ) and getside ( AmDepot ) = 1 and GetSide ( AmFac ) = 1 do
   0: LD_INT 1
   2: PPUSH
   3: CALL_OW 302
   7: PUSH
   8: LD_INT 2
  10: PPUSH
  11: CALL_OW 302
  15: AND
  16: PUSH
  17: LD_INT 2
  19: PPUSH
  20: CALL_OW 316
  24: AND
  25: PUSH
  26: LD_INT 1
  28: PPUSH
  29: CALL_OW 255
  33: PUSH
  34: LD_INT 1
  36: EQUAL
  37: AND
  38: PUSH
  39: LD_INT 2
  41: PPUSH
  42: CALL_OW 255
  46: PUSH
  47: LD_INT 1
  49: EQUAL
  50: AND
  51: IFFALSE 498
  53: GO 55
  55: DISABLE
// begin enable ;
  56: ENABLE
// begin if ThreatLevel < 1 then
  57: LD_EXP 1
  61: PUSH
  62: LD_INT 1
  64: LESS
  65: IFFALSE 101
// begin ChasisSelection = us_light_wheeled ;
  67: LD_ADDR_LOC 1
  71: PUSH
  72: LD_INT 1
  74: ST_TO_ADDR
// EngineSelection = rand ( 1 , 2 ) ;
  75: LD_ADDR_LOC 2
  79: PUSH
  80: LD_INT 1
  82: PPUSH
  83: LD_INT 2
  85: PPUSH
  86: CALL_OW 12
  90: ST_TO_ADDR
// WeaponSelection = us_machine_gun ;
  91: LD_ADDR_LOC 3
  95: PUSH
  96: LD_INT 2
  98: ST_TO_ADDR
// end else
  99: GO 272
// if threatLevel < 3 then
 101: LD_EXP 1
 105: PUSH
 106: LD_INT 3
 108: LESS
 109: IFFALSE 171
// begin ChasisSelection = [ us_light_wheeled , us_medium_tracked ] ;
 111: LD_ADDR_LOC 1
 115: PUSH
 116: LD_INT 1
 118: PUSH
 119: LD_INT 3
 121: PUSH
 122: EMPTY
 123: LIST
 124: LIST
 125: ST_TO_ADDR
// EngineSelection = rand ( 1 , 2 ) ;
 126: LD_ADDR_LOC 2
 130: PUSH
 131: LD_INT 1
 133: PPUSH
 134: LD_INT 2
 136: PPUSH
 137: CALL_OW 12
 141: ST_TO_ADDR
// WeaponSelection = [ us_machine_gun , us_light_gun ] [ rand ( 1 , 2 ) ] ;
 142: LD_ADDR_LOC 3
 146: PUSH
 147: LD_INT 2
 149: PUSH
 150: LD_INT 3
 152: PUSH
 153: EMPTY
 154: LIST
 155: LIST
 156: PUSH
 157: LD_INT 1
 159: PPUSH
 160: LD_INT 2
 162: PPUSH
 163: CALL_OW 12
 167: ARRAY
 168: ST_TO_ADDR
// end else
 169: GO 272
// if threatlevel < 10 then
 171: LD_EXP 1
 175: PUSH
 176: LD_INT 10
 178: LESS
 179: IFFALSE 247
// begin ChasisSelection = [ us_medium_wheeled , us_medium_tracked , us_medium_tracked ] [ difficulty ] ;
 181: LD_ADDR_LOC 1
 185: PUSH
 186: LD_INT 2
 188: PUSH
 189: LD_INT 3
 191: PUSH
 192: LD_INT 3
 194: PUSH
 195: EMPTY
 196: LIST
 197: LIST
 198: LIST
 199: PUSH
 200: LD_OWVAR 67
 204: ARRAY
 205: ST_TO_ADDR
// EngineSelection = 2 ;
 206: LD_ADDR_LOC 2
 210: PUSH
 211: LD_INT 2
 213: ST_TO_ADDR
// WeaponSelection = [ us_light_gun , us_double_gun , us_gatling_gun ] [ rand ( 1 , 3 ) ] ;
 214: LD_ADDR_LOC 3
 218: PUSH
 219: LD_INT 3
 221: PUSH
 222: LD_INT 5
 224: PUSH
 225: LD_INT 4
 227: PUSH
 228: EMPTY
 229: LIST
 230: LIST
 231: LIST
 232: PUSH
 233: LD_INT 1
 235: PPUSH
 236: LD_INT 3
 238: PPUSH
 239: CALL_OW 12
 243: ARRAY
 244: ST_TO_ADDR
// end else
 245: GO 272
// ChasisSelection = [ us_medium_tracked , us_medium_tracked , us_heavy_tracked ] [ difficulty ] ;
 247: LD_ADDR_LOC 1
 251: PUSH
 252: LD_INT 3
 254: PUSH
 255: LD_INT 3
 257: PUSH
 258: LD_INT 4
 260: PUSH
 261: EMPTY
 262: LIST
 263: LIST
 264: LIST
 265: PUSH
 266: LD_OWVAR 67
 270: ARRAY
 271: ST_TO_ADDR
// EngineSelection = 2 ;
 272: LD_ADDR_LOC 2
 276: PUSH
 277: LD_INT 2
 279: ST_TO_ADDR
// WeaponSelection = us_double_gun ;
 280: LD_ADDR_LOC 3
 284: PUSH
 285: LD_INT 5
 287: ST_TO_ADDR
// end ; MakeMe = [ ChasisSelection , EngineSelection , control_computer , WeaponSelection ] ;
 288: LD_ADDR_LOC 4
 292: PUSH
 293: LD_LOC 1
 297: PUSH
 298: LD_LOC 2
 302: PUSH
 303: LD_INT 3
 305: PUSH
 306: LD_LOC 3
 310: PUSH
 311: EMPTY
 312: LIST
 313: LIST
 314: LIST
 315: LIST
 316: ST_TO_ADDR
// display_strings := [ Make Me: ] ;
 317: LD_ADDR_OWVAR 47
 321: PUSH
 322: LD_STRING Make Me:
 324: PUSH
 325: EMPTY
 326: LIST
 327: ST_TO_ADDR
// if CanBeConstructed ( AmFac , MakeMe [ 1 ] , MakeMe [ 2 ] , MakeMe [ 3 ] , MakeMe [ 4 ] ) and CostOfVehicle ( MakeMe [ 1 ] , MakeMe [ 2 ] , MakeMe [ 3 ] , MakeMe [ 4 ] ) [ 1 ] <= GetResources ( GetBase ( AmDepot ) ) [ 1 ] then
 328: LD_INT 2
 330: PPUSH
 331: LD_LOC 4
 335: PUSH
 336: LD_INT 1
 338: ARRAY
 339: PPUSH
 340: LD_LOC 4
 344: PUSH
 345: LD_INT 2
 347: ARRAY
 348: PPUSH
 349: LD_LOC 4
 353: PUSH
 354: LD_INT 3
 356: ARRAY
 357: PPUSH
 358: LD_LOC 4
 362: PUSH
 363: LD_INT 4
 365: ARRAY
 366: PPUSH
 367: CALL_OW 448
 371: PUSH
 372: LD_LOC 4
 376: PUSH
 377: LD_INT 1
 379: ARRAY
 380: PPUSH
 381: LD_LOC 4
 385: PUSH
 386: LD_INT 2
 388: ARRAY
 389: PPUSH
 390: LD_LOC 4
 394: PUSH
 395: LD_INT 3
 397: ARRAY
 398: PPUSH
 399: LD_LOC 4
 403: PUSH
 404: LD_INT 4
 406: ARRAY
 407: PPUSH
 408: CALL_OW 449
 412: PUSH
 413: LD_INT 1
 415: ARRAY
 416: PUSH
 417: LD_INT 1
 419: PPUSH
 420: CALL_OW 274
 424: PPUSH
 425: CALL_OW 279
 429: PUSH
 430: LD_INT 1
 432: ARRAY
 433: LESSEQUAL
 434: AND
 435: IFFALSE 480
// ComConstruct ( AmFac , MakeMe [ 1 ] , MakeMe [ 2 ] , MakeMe [ 3 ] , MakeMe [ 4 ] ) ;
 437: LD_INT 2
 439: PPUSH
 440: LD_LOC 4
 444: PUSH
 445: LD_INT 1
 447: ARRAY
 448: PPUSH
 449: LD_LOC 4
 453: PUSH
 454: LD_INT 2
 456: ARRAY
 457: PPUSH
 458: LD_LOC 4
 462: PUSH
 463: LD_INT 3
 465: ARRAY
 466: PPUSH
 467: LD_LOC 4
 471: PUSH
 472: LD_INT 4
 474: ARRAY
 475: PPUSH
 476: CALL_OW 125
// if threatlevel > 100 then
 480: LD_EXP 1
 484: PUSH
 485: LD_INT 100
 487: GREATER
 488: IFFALSE 498
// threatlevel = 15 ;
 490: LD_ADDR_EXP 1
 494: PUSH
 495: LD_INT 15
 497: ST_TO_ADDR
// end ;
 498: END
