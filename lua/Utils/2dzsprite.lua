sprite2dzanimclass = {};
function sprite2dzanimclass.make(MANAGER,START_FRAME,FRAME_COUNT,DIR_OFFSET,FRAME_TIME)
	local id = SPRITE2DZ_ADDANIM(MANAGER.ID,START_FRAME,FRAME_COUNT,DIR_OFFSET,FRAME_TIME);
	return {ID=id,START=START_FRAME,COUNT=FRAME_COUNT,DIR_OFF=DIR_OFFSET,TIME=FRAME_TIME}; 
end;

sprite2dztextureclass = {};
function sprite2dztextureclass.make(MANAGER,IMAGE1,IMAGE2,FRAME_COUNT,FRAME_WIDTH,FRAME_HEIGHT,SHADOW_WIDTH,SHADOW_HEIGHT,IMAGE3)
	local id = SPRITE2DZ_ADDTEXTURE(MANAGER.ID,IMAGE1,IMAGE2,FRAME_COUNT,FRAME_WIDTH,FRAME_HEIGHT,SHADOW_WIDTH,SHADOW_HEIGHT,IMAGE3);
	return {ID=id,IMG1=IMAGE1,IMG2=IMAGE2,FC=FRAME_COUNT,FW=FRAME_WIDTH,FH=FRAME_HEIGHT,SFW=SHADOW_WIDTH,SFH=SHADOW_HEIGHT,IMG3=IMAGE3}; 
end;

sprite2dzclass = {};
function sprite2dzclass.make(MANAGER,X,Y,Z,TEXTURE,FRAME,VISIBLE)
	local id = SPRITE2DZ_ADDSPRITE(MANAGER.ID,X,Y,Z,TEXTURE,FRAME,VISIBLE);
	return {ID=id,MAN=MANAGER,XPOS=X,YPOS=Y,ZPOS=Z,TEX=TEXTURE,F=FRAME,V=VISIBLE,edit=sprite2dzclass.edit,
		setXY=sprite2dzclass.setXY,setZ=sprite2dzclass.setZ,setFrame=sprite2dzclass.setFrame,setVisible=sprite2dzclass.setVisible,
		setAnim = sprite2dzclass.setAnim, unsetAnim = sprite2dzclass.unsetAnim}; 
end;

function sprite2dzclass:edit(X,Y,Z,TEXTURE,FRAME,VISIBLE)
	self.XPOS = X;
	self.YPOS = Y;
	self.ZPOS = Z;
	self.TEX  = TEXTURE;
	self.F    = FRAME;
	self.V    = VISIBLE;

	SPRITE2DZ_EDITSPRITE(self.MAN.ID,self.ID,self.XPOS,self.YPOS,self.ZPOS,self.TEX,self.F,self.V);
end;

function sprite2dzclass:setXY(X,Y)
	self.XPOS = X;
	self.YPOS = Y;

	SPRITE2DZ_EDITSPRITE_XY(self.MAN.ID,self.ID,X,Y);
end;

function sprite2dzclass:setZ(VALUE)
	self.ZPOS = VALUE;

	SPRITE2DZ_EDITSPRITE_Z(self.MAN.ID,self.ID,VALUE);
end;

function sprite2dzclass:setFrame(VALUE)
	self.F = VALUE;

	SPRITE2DZ_EDITSPRITE_FRAME(self.MAN.ID,self.ID,VALUE);
end;

function sprite2dzclass:setVisible(VALUE)
	self.V = VALUE;

	SPRITE2DZ_EDITSPRITE_VISIBLE(self.MAN.ID,self.ID,VALUE);
end;

function sprite2dzclass:setAnim(ANIM,DIR,LOOP)
	SPRITE2DZ_SETSPRITEANIM(self.MAN.ID,self.ID,ANIM.ID,DIR,LOOP);
end;

function sprite2dzclass:unsetAnim()
	SPRITE2DZ_UNSETSPRITEANIM(self.MAN.ID,self.ID);
end;

sprite2dzmanagerclass = {};

function sprite2dzmanagerclass.make()
	return {ID=SPRITE2DZ_CREATE(), free = sprite2dzmanagerclass.free, addTexture = sprite2dzmanagerclass.addTexture, 
		addSprite = sprite2dzmanagerclass.addSprite, render = sprite2dzmanagerclass.render, addAnim = sprite2dzmanagerclass.addAnim,
		needRender = sprite2dzmanagerclass.needRender, setPause = sprite2dzmanagerclass.setPause};
end;


function sprite2dzmanagerclass:free() -- Free's EXE Internals but not this class
	SPRITE2DZ_FREE(self.ID);
end;

function sprite2dzmanagerclass:addTexture(IMAGE1,IMAGE2,FRAME_COUNT,FRAME_WIDTH,FRAME_HEIGHT,SHADOW_WIDTH,SHADOW_HEIGHT,IMAGE3)
	return sprite2dztextureclass.make(self,IMAGE1,IMAGE2,FRAME_COUNT,FRAME_WIDTH,FRAME_HEIGHT,SHADOW_WIDTH,SHADOW_HEIGHT,IMAGE3);
end;

function sprite2dzmanagerclass:addSprite(X,Y,Z,TEXTURE,FRAME,VISIBLE)
	return sprite2dzclass.make(self,X,Y,Z,TEXTURE,FRAME,VISIBLE);
end;

function sprite2dzmanagerclass:addAnim(START_FRAME,FRAME_COUNT,DIR_OFFSET,FRAME_TIME)
	return sprite2dzanimclass.make(self,START_FRAME,FRAME_COUNT,DIR_OFFSET,FRAME_TIME);
end;

function sprite2dzmanagerclass:render(GLSL,RENDERSHADOWS)
	SPRITE2DZ_RENDER(self.ID,GLSL.ID,RENDERSHADOWS);
end;

function sprite2dzmanagerclass:needRender()
	return SPRITE2DZ_NEEDRENDER(self.ID);
end;

function sprite2dzmanagerclass:setPause(VALUE)
	SPRITE2DZ_SETPAUSE(self.ID,VALUE);
end;

----------------------------------------------------------

