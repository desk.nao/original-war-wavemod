custom_dialog = "test"

function onCustomCommand()

  if (ccSystem.DATA[2] == 2) then

	local dial = ''
	for i = 5, 1, -1 do
		local label = game.chat.labels[i]
		local text = getText(label):match('%]%s*(.*)')
		if text then
			dial = dial .. text .. ' '
		end
	end
	
	OW_SETTING_WRITE('CustomStrings', 'Dialog', dial:sub(1, -2))

	for i = 1,5 do
	  setText(game.chat.labels[i],'');
	end;
		
	local custom_string = OW_SETTING_READ_STRING('CustomStrings', 'Dialog')
	if custom_string:find('< ', 1, true) then
		game.chat.labels.doAddChat(1, custom_string:sub(3))
		for i = 2, 4 do
			local prefix = ('< '):rep(i)
			if custom_string:find(prefix, 1, true) then
				game.chat.labels.doAddChat(1, custom_string:sub(2 * i + 1))
			else
				break
			end
		end
	end

	custom_dialog = getText(game.chat.labels[1])
	--local duration = 3+(string.len(custom_dialog)/20)
	--setText(SearchEditBox.debog,'Duration: '..duration)
	--AddSingleUseTimer(duration, 'NAO_VANILLA_DIALOGS();')
	OW_XICHT_SET(dyEx.obj.ID, dialog.objectives.customtext.ID)
	setVisible(game.xicht, true)
	setVisible(game.xicht.say, true)
	set_Colour(game.xicht.say.ID, PROP_FONT_COL, COLOURS[ccSystem.DATA[4] + 1])
	
	progress = 0;
	SomeCallback.enabled = true;
	setText(game.xicht.say, '')--custom_dialog)
	setText(game.chat.labels[1], '')
	
	end
	
	if (ccSystem.DATA[2] == 3) then	
		StartQuery(string.sub(getText(game.chat.labels[4]),string.find(getText(game.chat.labels[4]),']')+3),string.sub(getText(game.chat.labels[3]),string.find(getText(game.chat.labels[3]),']')+3),string.sub(getText(game.chat.labels[2]),string.find(getText(game.chat.labels[2]),']')+3),string.sub(getText(game.chat.labels[1]),string.find(getText(game.chat.labels[1]),']')+3)); 
	end;
	if (ccSystem.DATA[2] == 4) then	
		StartObjectives(string.sub(getText(game.chat.labels[1]),string.find(getText(game.chat.labels[1]),']')+3)); 
	end;
end;


ccSystem.add('onCustomCommand();');

function NAO_VANILLA_DIALOGS()
AddSingleUseTimer(1.5,"OW_XICHT_SET(game.xicht.ID,game.xicht.say.ID);")
OW_CUSTOM_COMMAND(1003,0,0,0,0);
OW_CUSTOM_COMMAND(3808,0,0,0,0);
SomeCallback.enabled = false;
setVisible(game.xicht.say,false)
setVisible(game.xicht,false)
end;

function IdentActiveUnit(un, side)
OW_CUSTOM_COMMAND(1010, un, side);
end;

MorgansCallbacks = listclass.make();

function MorgansCallbacks:addCallback(CALLBACK,ENABLED)
  CALLBACK.enabled = ENABLED;
  self:add(CALLBACK);
  CALLBACK.ID = self.COUNT;
  return CALLBACK;
end;

function MorgansCallbacks:tick(FRAMETIME)
  local c;
  for i = 1,self.COUNT do
    c = self:get(i);
	
    if c.enabled then
      c:tick(FRAMETIME);
	  setText(SearchEditBox.debog,'ON')
    elseif c.enabled == false then
	  setText(SearchEditBox.debog,'OFF')
	end
  end;
end;

SGUI_register_tick_callback('MorgansCallbacks:tick(%frametime)');

SomeCallback = {};
progress = 0;
function SomeCallback:tick(FRAMETIME);
  local length = string.len(custom_dialog)
  progress = progress + 35*FRAMETIME;
  if string.len(getText(game.xicht.say)) < length+1 then
    setText(game.xicht.say,string.sub(custom_dialog,1,progress))
  end
  if string.len(getText(game.xicht.say)) > length-1 then
    self.enabled = false;
	AddSingleUseTimer(2, 'NAO_VANILLA_DIALOGS();')
  end
end;

SomeCallback = MorgansCallbacks:addCallback(SomeCallback,false);
SomeCallback.enabled = false;

include('devmode')