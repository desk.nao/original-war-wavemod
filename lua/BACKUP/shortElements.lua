function shortButton(X,Y,W,TEXT,COLOUR,CALLBACK,SUB_HINT)
  return getImageButtonEX(menu[1],anchorNone,XYWH(X,getHeight(menu[1])-Y,W,25),TEXT,'',CALLBACK,SKINTYPE_BUTTON,{font_colour_disabled=GRAY(127),colour=COLOUR,font_colour=COLOUR,text_halign=ALIGN_MIDDLE,callback_mouseclick=CALLBACK,hint=SUB_HINT});
end;

function shortButton_Tech(X,Y,W,TEXT,COLOUR,CALLBACK,SUB_HINT)
  return getImageButtonEX(dynamic.commands[35],anchorNone,XYWH(X,getHeight(menu[1])-Y,W,25),TEXT,'',CALLBACK,SKINTYPE_BUTTON,{font_colour_disabled=GRAY(127),colour=COLOUR,font_colour=COLOUR,text_halign=ALIGN_MIDDLE,callback_mouseclick=CALLBACK,hint=SUB_HINT});
end;

function shortEdit(X,Y,W,TEXT,COLOUR,SUB_HINT)
  return getEditEX(menu[1],anchorNone,XYWH(X,getHeight(menu[1])-Y,W,25),CourierNew_12B,'',TEXT,COLOURS_DIALOG_EDIT,{colour=COLOUR,callback_keypress="if (%k == VK_RETURN) then clearFocus(); end;",hint=SUB_HINT});
end;

function shortLabel(X,Y,W,TEXT,COLOUR,SUB_HINT)
  return getLabelEX(menu[1],anchorNone,XYWH(X,getHeight(menu[1])-Y,W,25),Tahoma_13,TEXT,{hint=SUB_HINT, font_colour=COLOUR, text_halign=ALIGN_MIDDLE, shadowtext=true, automaxwidth=195, autosize=true, wordwrap=true, highlight=false, VALUE=1});
end;

function shortLabel_Tech(X,Y,W,TEXT,COLOUR,SUB_HINT)
  return getLabelEX(dynamic.commands[35],anchorNone,XYWH(X,getHeight(menu[1])-Y,W,25),Tahoma_13,TEXT,{hint=SUB_HINT, font_colour=COLOUR, text_halign=ALIGN_MIDDLE, shadowtext=true, automaxwidth=195, autosize=true, wordwrap=true, highlight=false, VALUE=1});
end;

function shortElement(X,Y,W,H,C1,C2,C3,SUB_TEXTURE,SUB_HINT)
  return getElementEX(menu[1],anchorLTR,XYWH(X,getHeight(menu[1])-Y,W,H),true,{callback_mouseclick=C1,callback_mouseover=C2,callback_mouseleave=C3,texture=SUB_TEXTURE,hint=SUB_HINT,});
end;

function shortElement_Tech(X,Y,W,H,C1,C2,C3,SUB_TEXTURE,SUB_HINT)
  return getElementEX(dynamic.commands[35],anchorLTR,XYWH(X,getHeight(menu[1])-Y,W,H),true,{callback_mouseclick=C1,callback_mouseover=C2,callback_mouseleave=C3,texture=SUB_TEXTURE,hint=SUB_HINT,});
end;

function logLabel(TYPE,X,Y,W,TEXT,COLOUR,SUB_HINT,CALLBACK,SP1,SP2)
  commands['COUNT'] = commands['COUNT']+1;
  
  if TYPE == 1 then 
    commands[22][p].sub_c = commands[22][p].sub_c +1;
    return getLabelEX(dynamic.commands[5],anchorLTR,XYWH(X,Y,W,25),Tahoma_13,TEXT,{sub_p = p, callback_mouseclick=CALLBACK, V1=SP1, V2=SP2, visible=true, hint=SUB_HINT, font_colour=COLOUR, highlight=true, text_halign=ALIGN_LEFT, shadowtext=true, automaxwidth=195, autosize=true, wordwrap=true});  
    
  elseif TYPE == 2 then p = commands['COUNT']-1;
    return getLabelEX(dynamic.commands[5],anchorLTR,XYWH(X,Y,W,25),BankGotic_14,TEXT,{sub_c = 1, callback_mouseclick=--[["collapse("..p..");"]]CALLBACK, V1=SP1, V2=SP2, visible=true, hint=SUB_HINT, font_colour=WHITE(), colour1=BLACK(), text_halign=ALIGN_LEFT, shadowtext=true, automaxwidth=195, autosize=true, wordwrap=true});
  end;

end;

function isTable(t) return type(t) == 'table' end;