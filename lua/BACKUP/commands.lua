--[[ Dynamic interface -- Commands
	 Updated: 05/03/2023 - 28/07/2022 by Morgan
--]]
	

dynamic.commands    = {};



include('Dynamic/unitProperties');



PropertiesManager = createPropertiesManager()
PropertiesManager.clear(1)

function dynamic.unitproperties(ID)

  local info = {"CLASSES","SKILLS","ATTRIBUTES","SIDES","LIFE"};
  
  --sgui_deletechildren(dynamic.commands[9].ID);

  --for i = 1,#info do
  --  dynamic.commands[9][info[i]] = {};
  --end;
  
  local TYPE = {'CLASSES','SKILLS','ATTRIBUTES','SIDES','LIFE'}
  local CAT = {CLASSES,SKILLS,ATTRIBUTES,SIDES,LIFE}
  
  local CUSTOM = {TYPE[ID],CAT[ID]}
  local parent = dynamic.commands[9]
  
  for i = 1,#CUSTOM[2] do
    local callback = {
	  'OW_CUSTOM_COMMAND(510,'..CUSTOM[2][i][2]..');',
	  '',
	  '',
	  'OW_CUSTOM_COMMAND(511,'..(i-1)..');',
	  'OW_CUSTOM_COMMAND(515,LIFE['..i..'][2]);'
	  }
  
    --dynamic.commands[9][CUSTOM[1]][i] 
    menu[12][i] = getLabelEX(menu[12],anchorNone,XYWH(5,2.5+((i-1)*15),171,15),Tahoma_13,CUSTOM[2][i][1],{text_halign=ALIGN_LEFT, callback_mouseclick=callback..' dynamic.change('..ID..','..i..',%b);', automaxwidth=190-12, autosize=true, wordwrap=true, highlight=true});
  end

end;

function dynamic.change(TYPE,SKILL,CLICK)
  local CALC = 0;
  if CLICK == 1 then CALC = -1; else CALC = 1; end;
  
  local INFO = {commands['COUNT'], "class","skills","attributes","side","health"};
  
  if TYPE == 2 then -- SKILLS
    SKILLS[SKILL][2] = SKILLS[SKILL][2]+CALC;
	if SKILLS[SKILL][2] > 10 then SKILLS[SKILL][2] = 0;
	  elseif SKILLS[SKILL][2] < 0 then SKILLS[SKILL][2] = 10;
	end;
	OW_CUSTOM_COMMAND(512,SKILL,SKILLS[SKILL][2]);
  end;
  
  if TYPE == 3 then -- ATTRIBUTES
    ATTRIBUTES[SKILL][2] = ATTRIBUTES[SKILL][2] + CALC;
	if ATTRIBUTES[SKILL][2] < 0 then ATTRIBUTES[SKILL][2] = 0; end;
    OW_CUSTOM_COMMAND(513,ATTRIBUTES[1][2],ATTRIBUTES[2][2]); 
  end;
  
  commands[22][INFO[1]] = logLabel(2,2.5,commands.getY(INFO[1]-1),getWidth(dynamic.commands[5])-14.25,commands['COUNT2']..' - '..getText(commands[4][1]),WHITE(),dynamic.commands.list[selected_id].hint,get_Callback(dynamic.commands.list[selected_id].ID, CALLBACK_MOUSECLICK), 1, 1 );
  commands['COUNT2'] = commands['COUNT2'] +1;
  sgui_autosizecheck(commands[22][INFO[1]].ID)
  
  commands[22][INFO[1]+1] = logLabel(1,5,commands.getY(INFO[1]),0,'Modified unit\'s '..INFO[TYPE+1]..'.',sgui_get(commands[4][1].ID,PROP_FONT_COL),'','', 1, 1);
  sgui_autosizecheck(commands[22][INFO[1]+1].ID);
end;

function dynamic.show(VIS)
  local data = {
    {xicht_human, dynamic.commands[7], dynamic.commands[8], dynamic.commands[9], dynamic.commands[10]},
    {false, true}
  }

  local visibility = data[2][VIS]
  for i = 1, #data[1] do
    setVisible(data[1][i], visibility)
  end

  if VIS == 2 then
    setVisible(dynamic.commands.run, false)
  end
end


selected_id = 1;

include('Dynamic/shortElements');

-- LIBRARY
commands    = {};
commands['COUNT'] = 1;
commands['COUNT2'] = 1;


function showbeh(value)
  for i = 30,34 do setVisible(dynamic.commands[i],value); end;
end;


function collapse(id)
  for i = id+1,id+commands[22][id].sub_c-1 do 
    AddEventFade(commands[22][i].ID,0,0.75);
	AddEventSlideY(commands[22][i].ID,getY(commands[22][id]),0.75);
    --setVisible(commands[22][i],not getVisible(commands[22][i]));
  end;
  AddSingleUseTimer(1,'coll('..(1+id+commands[22][id].sub_c-1)..');');

end;

function coll(idd)
AddEventSlideY(commands[22][idd].ID,27.5+getY(commands[22][idd-1]),0.25,'cooo('..(idd+1)..');');

end;

function cooo(id)
setText(SearchEditBox.debog,'');
  for i = id,#commands[22] do 

   if i == id then pas = 25; else pas = getHeight(commands[22][(i-1)]); end;
   AddSingleUseTimer(i/10,'setY(commands[22]['..i..'],getY(commands[22]['..(i-1)..'])+('..pas..'));');
   
   setText(SearchEditBox.debog, getText(SearchEditBox.debog)..' | '..(i-1) );
  end;
end;



include('Dynamic/interface');

commands[14]["LOC"] = {103,  109, 121, 123, 125, 157, 161, 127, 145, 165, 111, 163, 233, 167, 159, 171, 173, 169, 189, 101, 183, 185, 237, 187, 191, 193, 139, 205, 207, 209, 211, 138, 143, 231, 105, 141, 213, 147, 225, 227, 241, 239, 243, 235, 107, 97, 99, 113, 115, 117, 247, 249, 251, 198, 200, 202, 149, 151, 153, 175, 177, 179, 215, 217, 219, 129, 131, 133, 223, 245, 229, 195};

function commands.getY(ID)
	if ID < 1 then return 2.5; end;
	if ID > 0 then return getElementBottom(commands[22][ID]); end;
end;

function Correct_find(d_loc)
if string.match(d_loc,"[..]",0) == nil then return 6 else return 0; end;
end;

function AddIt(TEXT,TYPE)
  local labels = {'Enabled','Researched','Disabled'};
  
  if commands['COUNT'] > 1 then 
    commands[22][commands['COUNT']] = logLabel(1,5,commands.getY(commands['COUNT']-1),0,labels[TYPE]..' technology:\n'..TEXT,COLOURS[special_value[21][1]+1],'','', 1, 1); 
    sgui_autosizecheck(commands[22][commands['COUNT']-1].ID);
  end;
  
end;

  function debog_button(B,i)
    local results = { {2,'_re'}, {3,'_di'}, {1,'_en'} };
	
    setTexture(commands[14][i],'technologies/'..(i-9)..results[B+1][2]..'.png'); 
    AddIt(loc(1400+(i-9)),results[B+1][1]); 
  end;



for i = 10, 81 do 
  local x = (i - 10) % 6 * 30 - 30
  local y = math.floor((i - 10) / 6) * 30
  local x_int = math.modf(11.5 + x)
  commands[14][i] = shortElement_Tech(30 + x_int, 190 - y, 25, 25, 

	[[ debog_button(%b,]]..i..[[);
	
	commands[14][]]..i..[[].VALUE = getTexture(commands[14][]]..i..[[]); 
	OW_CUSTOM_COMMAND(505,(]]..i..[[-9),%b,special_value[14][1]);]],
	
	[[if getTexture(commands[14][(]]..i..[[)]) == 'technologies/(]]..i..[[-9)_en.png' then 
		setTexture(commands[14][(]]..i..[[)],'technologies/(]]..i..[[-9)_mo.png'); 
	end;]],

	[[if getTexture(commands[14][(]]..i..[[)]) == 'technologies/(]]..i..[[-9)_mo.png' then 
		setTexture(commands[14][(]]..i..[[)],'technologies/(]]..i..[[-9)_en.png');
	end;]],
	
	'technologies/'..(i-9)..'_en.png',
	loc(1400+(i-9))..'\n\n'..string.sub(loc(commands[14]["LOC"][i-9]),
	Correct_find(commands[14]["LOC"][i-9]),
	string.len(loc(commands[14]["LOC"][i-9])))..'\n\nLeft click: set to researched.\nRight click: set to disabled.\nMiddle click: reset to enabled.')
	
end

function TechSettings(technology, side, state)
  local texture
  if state == 2 then
    texture = 'technologies/' .. technology .. '_re.png'
  elseif state == 0 then
    texture = 'technologies/' .. technology .. '_di.png'
  end
  setTexture(commands[14][technology + 9], texture)
  table.insert(commands[210][special_value[14][1]], technology, texture)
end

commands[22] = {};
  
for i = 1,21 do
  if commands[i] ~= nil then 
    for y = 1,#commands[i] do
	  setVisible(commands[i][y],false);
	end;
  end;
end;
  
  commands["outputs"] = {
  
  };
  

  
-- OW_CUSTOM_COMMAND(5003,HXS[1],HXS[2],(commands[1][1].VALUE-1));
  
  
function RunCommand(selected_id)
  local COUNT,dy = commands['COUNT'],dynamic.commands[5];
  
  -- Black & White Headers
  if selected_id < 100 then 
    commands[22][COUNT] = logLabel(2,2.5,commands.getY(COUNT-1),getWidth(dy)-14.25,commands['COUNT2']..' - '..dynamic.commands.all[selected_id][1],WHITE(),dynamic.commands.list[selected_id].hint,get_Callback(dynamic.commands.list[selected_id].ID, CALLBACK_MOUSECLICK), 1, 1 );
	commands['COUNT2'] = commands['COUNT2'] +1;
	sgui_autosizecheck(commands[22][COUNT].ID)
  end;
  
  -- Callbacks Mouse Click (Run Command)
  if selected_id == 1 then
	TriggerMapVision();
  elseif selected_id == 2 then
    commands[22][COUNT+1] = logLabel(1,5,commands.getY(COUNT),0,'Switched '..Labels[commands[2][1].VALUE]..' player to '..Labels[commands[2][2].VALUE]..'.',COLOURS[commands[2][1].VALUE],'','', 1, 1);
    sgui_autosizecheck(commands[22][COUNT+1].ID);
	OW_CUSTOM_COMMAND(504,commands[2][1].VALUE-1,commands[2][2].VALUE-1);
  elseif selected_id == 3 then 
    CreateResources();
  elseif selected_id == 8 then
    CreateUnit();
  elseif selected_id == 9 then
    VEHICLES:Create();
  elseif selected_id == 20 then 
    SpawnEntity(); 

  -- Map Vision
  elseif selected_id == 101 then
    OW_CUSTOM_COMMAND(1013,getText(commands[1][2]),commands[1][1].VALUE-1);
	
    commands[22][COUNT] = logLabel(1,5,commands.getY(COUNT-1),0,'Vision for '..Labels[commands[1][1].VALUE]..' at '..HXS[1]..' | '..HXS[2]..'\nRange: '..getText(commands[1][2])..' hexagons.',COLOURS[commands[1][1].VALUE],'Click to remove vision.','', 1, 1);
	commands[22][COUNT].X = HXS[1];
	commands[22][COUNT].Y = HXS[2];
	local CX,CY = commands[22][COUNT].X,commands[22][COUNT].Y;
	set_Callback(commands[22][COUNT].ID,CALLBACK_MOUSECLICK,'VISION('..commands[22][COUNT].ID..',sgui_get(commands[22]['..COUNT..'].ID,PROP_STRIKETHROUGH),'..CX..','..CY..','..commands[1][1].VALUE..','..getText(commands[1][2])..');');
	
	sgui_autosizecheck(commands[22][COUNT].ID);
	
  elseif selected_id == 308 then
    commands[22][COUNT] = logLabel(1,5,commands.getY(COUNT-1),0,'Spawned unit at '..HXS[1]..' | '..HXS[2],COLOURS[units_data[12]+1],'Click to focus on co-ordinates.','', HXS[1], HXS[2]);
	sgui_autosizecheck(commands[22][COUNT].ID);
	set_Callback(commands[22][COUNT].ID,CALLBACK_MOUSECLICK,'OW_CUSTOM_COMMAND(503,'..commands[22][COUNT].V1..','..commands[22][COUNT].V2..');');
	commands[22][COUNT+1] = logLabel(1,5,commands.getY(COUNT),0,'<name surname> (class)',COLOURS[units_data[12]+1],'Click to focus on unit.','',1,1);
	sgui_autosizecheck(commands[22][COUNT+1].ID);
	
  
  -- Create Resources
  elseif selected_id == 301 then
    commands[22][COUNT] = logLabel(1,5,commands.getY(COUNT-1),0,'Sent '..special_value[3][1][special_value[3][1][1]+3]..' (Amount: '..special_value[3][2][special_value[3][2][1]+3]..')\nLocation: '..HXS[1]..' | '..HXS[2],RGB(175,175,175),'Click to focus on co-ordinates.','', HXS[1], HXS[2]);
    sgui_autosizecheck(commands[22][COUNT].ID);
	set_Callback(commands[22][COUNT].ID,CALLBACK_MOUSECLICK,'OW_CUSTOM_COMMAND(503,'..commands[22][COUNT].V1..','..commands[22][COUNT].V2..');');
  
  --elseif selected_id == 901 then
  
  end;
  
end;

Labels = {"Neutral", "Blue", "Yellow", "Red", "Cyan", "Orange", "Purple", "Green", "Gray", "Spectator"};

special_value = {
--[[Map Vision ]] {1,0}, -- Side, Range 
--[[Switch Side]] {1,0},
--[[Create Reso]] { {1,3,2,'Crates','Oil','Siberite'}, {1,6,4,'Random','1','2','3','4','5'}, {1,2,6,'YES','NO'}, {1,2,8,'YES','NO'} },
{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},
--[[Create Unit]] {1,{0,0,0,0},0},    -- Side, Levels, 
--[[Set Technol]] {1} -- Side
};

	  for y = 1,8 do 
		for i = 1,72 do  
		  table.insert(commands[210][y],i,getTexture(commands[14][i+9]));
	    end;
	  end;
	    
function set_specialValue(G_ID,ID,V)
  local sub_V, T;

	if isTable(V) then
	  T = V[1]
	else
	  T = V;
	end;

	if T == 0 then
	  sub_V = 1;
	elseif T == 1 then
	  sub_V = (-1);
	end;
  
  --[[MAP VISION ]] if G_ID == 1 then 

  if ID == 1 then -- SIDE
    special_value[G_ID][ID] = special_value[G_ID][ID]+sub_V;

	if special_value[G_ID][ID] > 9 then
	  special_value[G_ID][ID] = 1;
	elseif special_value[G_ID][ID] < 1 then
	  special_value[G_ID][ID] = 9;
	end;
	
    set_Colour(commands[1][1].ID,PROP_FONT_COL,RGB(0,0,0));
    set_Colour(commands[1][1].ID,PROP_FONT_COL_BACK,COLOURS[special_value[G_ID][ID]]);
    set_Colour(commands[1][1].ID,PROP_COL1,COLOURS[special_value[G_ID][ID]]);
    setText(commands[1][1],Labels[special_value[G_ID][ID]]);
    commands[1][1].VALUE = special_value[G_ID][ID];
  end;

  if ID == 2 then -- RANGE
    if V == 0 then special_value[G_ID][ID] = special_value[G_ID][ID] +5; 
	  else if special_value[G_ID][ID] > 4 then special_value[G_ID][ID] = special_value[G_ID][ID] -5; end;
	end;
      commands[1][2].VALUE = special_value[G_ID][ID];
      setText(commands[1][2],special_value[G_ID][ID]);
  end;
  
  end;

  --[[SWITCH SIDE]] if G_ID == 2 then 

    special_value[G_ID][ID] = special_value[G_ID][ID]+sub_V;

	if special_value[G_ID][ID] > 10 then
	  special_value[G_ID][ID] = 1;
	elseif special_value[G_ID][ID] < 1 then
	  special_value[G_ID][ID] = 10;
	end;
	
    set_Colour(commands[2][ID].ID,PROP_FONT_COL,RGB(0,0,0));
    set_Colour(commands[2][ID].ID,PROP_FONT_COL_BACK,COLOURS[special_value[G_ID][ID]]);
    set_Colour(commands[2][ID].ID,PROP_COL1,COLOURS[special_value[G_ID][ID]]);
    setText(commands[2][ID],Labels[special_value[G_ID][ID]]);
    commands[2][ID].VALUE = special_value[G_ID][ID];

  end;

  --[[CREATE RESO]] if G_ID == 3 then 

    special_value[G_ID][ID][1] = special_value[G_ID][ID][1] +1;
  
	if special_value[G_ID][ID][1] > special_value[G_ID][ID][2] then
	  special_value[G_ID][ID][1] = 1;
	elseif special_value[G_ID][ID][1] < 1 then
	  special_value[G_ID][ID][1] = special_value[G_ID][ID][2];
	end;

    setText(commands[3][special_value[G_ID][ID][3]],special_value[G_ID][ID][special_value[G_ID][ID][1]+3]);

  end;

  --[[UNIT PROPER]] if G_ID == 4 then
  if ID == 1 then -- CLASS
    special_value[G_ID][ID] = special_value[G_ID][ID] +sub_V;
	  if special_value[G_ID][ID] > 12 then special_value[G_ID][ID] = 1;
	  elseif special_value[G_ID][ID] < 1 then special_value[G_ID][ID] = 12;
	  end;
	  setText(commands[4][1],CLASSES[special_value[G_ID][ID]][1]);
  end;
  OW_CUSTOM_COMMAND(510,CLASSES[special_value[G_ID][ID]][2]);
  end;
  
end;




HXS = {50, 50}
H = {}
LOCAL = {}
LOCAL.HEX = {}

function LOCAL.HEX:collect(SIDE, X, Y)
  FH = OW_FINDHEX(X, Y)
  H = OW_HEXTOSCREEN(FH.X, FH.Y)
  if FH.FOUND == true then
    HXS = {FH.X, FH.Y};
    OW_CUSTOM_COMMAND(4448, HXS[1], HXS[2], SIDE)
  end;
  --OW_CUSTOM_COMMAND(99,getvalue(OWV_MYSIDE))
end;

xicht_human = getElementEX(menu[1],anchorNone,XYWH((getWidth(menu[1])/2)-46,100,80,100),false,{});
xicht_build = getElementEX(menu[1],anchorNone,XYWH((getWidth(menu[1])/2)-38,100,64,64),false,{});
xicht_vehic = getElementEX(menu[1],anchorNone,XYWH((getWidth(menu[1])/2)-46,100,80,64),false,{});