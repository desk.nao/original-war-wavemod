--[[ Dynamic interface
	 Updated: 05/07/2022 by Morgan
--]]

playDialogs = {};
playDialogs.dubs = getElementEX(nil,anchorNone,XYWH(ScrWidth-220,155+75,210,LayoutHeight-400),false,{type=TYPE_SCROLLBOX, scissor=true, colour1=BLACKA(150)});
playDialogs.heroes = getElementEX(nil,anchorNone,XYWH(ScrWidth-220,100,210,125),false,{type=TYPE_SCROLLBOX, scissor=true, colour1=BLACKA(150)});

playDialogs.caption   = getLabelEX(dyEx,anchorLT,XYWH(5,5,0,0),Tahoma_13,'',{automaxwidth=300, autosize=true, wordwrap=true,});
playDialogs.list  = {};
playDialogs.count = 0;

playDialogs.portraits = {};
playDialogs.portraits.count = 0;
--
playDialogs.heroes.names = {'Bobby Brandon','Lt. Jeff Brown','Connie Traverse','Andy Cornell','Cyrus Parker','Paul Khattam',
'Denis Peterson','Lucy Donaldson','Frank Forsyth','Gary Grant','Tim Gladstone','Ron Harrison','Hugh Stevens','John Macmillan',
'Joan Fergusson','Lisa Lawson','Arthur Powell','Dr. Peter Roth','Jeremy Sikorski','Cathy Simms','Peter Van Houten',
'Yamoko Kikuchi','void','Abdul Shariff','Rolf Bergkamp','Dietrich Gensher','Robert Farmer','Heike Steyer','Kurt Schmidt',
'Cheikh Omar','Raul Xavier','void','Caporal P. M. Belkov','Caporal I. M. Belkov','M. I. Borodin','Y. I. Burlak Gorky',
'A. E. Bystrov','Col. Pyotr Davidov','Dr. Lazar Dolgov','Col. Eisenstein','Konstantin Fadeev','Fyodor Furmanov',
'Cpt. Timur Gaydar','Dimitri Gladkov','Caporal Gleb','Caporal Gnyevko','Prof. Gossudarov','Caporal Grishko','Olga Kapitsova',
'Pvt. B. L. Karamazov','Y. P. Kirilenkova','Ilya Kovalyuk','Nikita Kozlov','Col. Ivan Kurin','Pvt. N. P. Kuzmov',
'Caporal Lipshchin','Mikhail Berezov','Col. Morozov','Leonid Oblukov','Cpt. Petrosyan','Caporal Petrovova','Major Platonov',
'Lt. Pokryshkin','Cpt. Sergey Popov','Prof. Scholtze','Cpt. Stolypin','Lev Titov','Col. Tsarytsyn','Cpt. Gorky','Marshal Yashin',
'void', 'Sci', 'Sci1', 'Sol1', 'Sol2', 'Sol3', 'Sol4', 'Sol5', 'Eng1', 'FEng1', 'FMech1', 'Off',
'Ar1', 'Ar2', 'ArSol1','void','ArSol2', 'ASci1', 'ASol1', 'FAr1', 'FMerc2', 'Friend', 'FRus1', 'FSci1', 'Merc1', 
'All', 'void', 'Off1', 'Off2', 'RFSol1', 'RSci1', 'RSol1', 'RSol2', 'Rus1', 'Rus2', 'Rus3', 'Russ',
'FSol1', 'Fsol2', 'FSol3', 'void', 'Fsol4', 'Mech1'};

playDialogs.scrollbox = AddElement({
	type=TYPE_SCROLLBAR,
	parent=playDialogs,
	anchor={top=true,bottom=true,left=false,right=true},
	visible=false,
	x=getX(playDialogs.dubs)+getWidth(playDialogs.dubs)-12,
	y=getY(playDialogs.dubs),
	width=12,
	height=LayoutHeight-400,
	colour1=Scrollbar_Colour1,
	colour2=Scrollbar_Colour2,
	texture2="scrollbar.png",
});

playDialogs.heroes.scrollbox = AddElement({
	type=TYPE_SCROLLBAR,
	parent=playDialogs,
	anchor={top=true,bottom=true,left=false,right=true},
	visible=false,
	x=getX(playDialogs.heroes)+getWidth(playDialogs.heroes)-12,
	y=getY(playDialogs.heroes),
	width=12,
	height=getHeight(playDialogs.heroes),
	colour1=Scrollbar_Colour1,
	colour2=Scrollbar_Colour2,
	texture2="scrollbar.png",
});

sgui_set(playDialogs.dubs.ID,PROP_SCROLLBAR,playDialogs.scrollbox.ID);
sgui_set(playDialogs.heroes.ID,PROP_SCROLLBAR,playDialogs.heroes.scrollbox.ID);

passed = 0;

function playDialogs.getY(ID)
	if ID < 1 then
        	return getElementBottom(playDialogs.caption)+15; end;
	if ID == 113 then 
			if passed == 0 then setY(playDialogs.list[114],0); end; end;
	if ID ~= 113 then
        	return getElementBottom(playDialogs.list[ID]); end;
end;

function playDialogs.createElement(TEXT,TYPE,SPEAKER,cusID)
	if TYPE == 1 then lab = getLabelEX(playDialogs.dubs,anchorLT,XYWH(5,playDialogs.getY(playDialogs.count-1),50,50),Tahoma_13,TEXT,{callback_mouseclick='OW_CUSTOM_COMMAND(3010,'..cusID..','..SPEAKER..');', callback_mouseleave='set_Colour(playDialogs.list['..playDialogs.count..'].ID,PROP_FONT_COL,RGB(224,224,224));', callback_mouseover='set_Colour(playDialogs.list['..playDialogs.count..'].ID,PROP_FONT_COL,RGB(255,255,153));', hint=playDialogs.count, automaxwidth=195, autosize=true, wordwrap=true,}); end;
	if TYPE == 2 then lab = getLabelEX(playDialogs.heroes,anchorLT,XYWH(15,playDialogs.getY(playDialogs.count-1),50,50),Tahoma_13,TEXT,{callback_mouseclick='LoadLinesCustom('..SPEAKER..');', font_colour=WHITE(), colour1=BLACKA(0), text_halign=ALIGN_LEFT, shadowtext=true, automaxwidth=195, autosize=true, wordwrap=true, hint=playDialogs.count..'SPEAKER: '..SPEAKER}); end;
	sgui_autosizecheck(lab.ID);
    return lab;
end;

function playDialogs.createPortrait(cusID)
	local lab = getElementEX(playDialogs.heroes,anchorNone,XYWH(2.5,15*playDialogs.portraits.count,12,15),true,{texture='/SGUI/Nao/heroes/'..playDialogs.heroes.names[1]..'.png',});	
	playDialogs.portraits.count = playDialogs.portraits.count +1;
	return lab 
end;

function playDialogs.add(TEXT,TYPE,SPEAKER,cusID)
    playDialogs.count = playDialogs.count + 1;
    playDialogs.list[playDialogs.count] = playDialogs.createElement(TEXT,TYPE,SPEAKER,cusID); 
	if TYPE == 2 then playDialogs.portraits[playDialogs.portraits.count] = playDialogs.createPortrait(playDialogs.portraits.count); end;
	return playDialogs.count;
end;

for i = 49001,49113 do playDialogs.add(loc(i),2,i-48999); end;

passed = 1;

playDialogs.all = { 
{'', 0, 0, 0 },
{'Bobby Brandon:', 16, 48005, 48006 },
{'Lt. Jeff Brown:', 19, 48005, 48006 },
{'Connie Traverse:', 22, 48005, 48006 },
{'Andy Cornell:', 23, 48005, 48006 },
{'Cyrus Parker:', 24, 48005, 48006 },
{'Paul Khattam:', 62, 48005, 48006 },
{'Denis Peterson:', 26, 48005, 48006 },
{'Lucy Donaldson:', 29, 48005, 48006 },

{'Frank Forsyth:', 37, 48005, 48006 },
{'Gary Grant:', 47, 48005, 48006 },
{'Tim Gladstone:', 50, 48005, 48006 },
{'Ron Harrison:', 55, 48005, 48006 },
{'Hugh Stevens:', 57, 48005, 48006 },
{'John Macmillan:', 58, 48005, 48006 },
{'Joan Fergusson:', 59, 48005, 48006 },
{'Lisa Lawson:', 69, 48005, 48006 },
{"Arthur Powell:", 82, 48005, 48176 },
{'Dr. Peter Roth:', 90, 48005, 48006 },
{'Jeremy Sikorski:', 103, 48005, 48006 },
{'Cathy Simms', 104, 48005, 48006 },
{'Peter Van Houten', 112, 48005, 48006 },
{'Yamoko Kikuchi', 115, 48005, 48006 },
{'', 0, 48005, 48006 },
{"Abdul Shariff:", 1, 48000, 48004 },
{'Rolf Bergkamp', 18, 48005, 48006 },
{'Dietrich Gensher', 27, 48005, 48006 },
{'Robert Farmer', 39, 48005, 48006 },
{'Heike Steyer', 56, 48005, 48006 },
{'Kurt Schmidt', 0, 48005, 48006 },
{"Cheikh Omar:", 77, 48194, 48199 },
{'Raul Xavier', 114, 48005, 48006 },
{'', 0, 48005, 48006 },
{'Caporal P. M. Belkov', 15, 48005, 48006 },
{'Caporal I. M. Belkov', 15, 48005, 48006 },
{'M. I. Borodin', 17, 48005, 48006 },
{'Y. I. Burlak Gorky', 20, 48005, 48006 },
{'A. E. Bystrov', 21, 48005, 48006 },
{'Col. Pyotr Davidov', 25, 48005, 48006 },
{'Dr. Lazar Dolgov', 28, 48005, 48006 },
{'Col. Eisenstein', 30, 48005, 48006 },
{'Konstantin Fadeev', 32, 48005, 48006 },
{'Fyodor Furmanov', 46, 48005, 48006 },
{'Cpt. Timur Gaydar', 48, 48005, 48006 },
{'Dimitri Gladkov', 0, 48005, 48006 },
{'Caporal Gleb', 0, 48005, 48006 },
{'Caporal Gnyevko', 0, 48005, 48006 },
{'Prof. Gossudarov', 0, 48005, 48006 },
{'Caporal Grishko', 0, 48005, 48006 },
{'Olga Kapitsova', 0, 48005, 48006 },
{'Pvt. B. L. Karamazov', 0, 48005, 48006 },
{'Y. P. Kirilenkova', 0, 48005, 48006 },
{'Ilya Kovalyuk', 0, 48005, 48006 },
{'Nikita Kozlov', 0, 48005, 48006 },
{'Col. Ivan Kurin', 0, 48005, 48006 },
{'Pvt. N. P. Kuzmov:', 67, 48200, 48217 },
{'Caporal Lipshchin', 0, 48005, 48006 },
{'Mikhail Berezov', 0, 48005, 48006 },
{'Col. Morozov', 0, 48005, 48006 },
{'Leonid Oblukov', 0, 48005, 48006 },
{'Cpt. Petrosyan', 0, 48005, 48006 },
{'Caporal Petrovova', 0, 48005, 48006 },
{'Major Platonov', 0, 48005, 48006 },
{'Lt. Pokryshkin', 0, 48005, 48006 },
{'Cpt. Sergey Popov', 0, 48005, 48006 },
{"Prof. Scholtze:", 100, 48176, 48193 },
{'Cpt. Stolypin', 0, 48005, 48006 },
{'Lev Titov', 0, 48005, 48006 },
{'Col. Tsarytsyn', 0, 48005, 48006 },
{'Cpt. Gorky', 0, 48005, 48006 },
{'Marshal Yashin', 0, 48005, 48006 },
{'', 0, 48005, 48006 },
{'Sci', 0, 48005, 48006 },
{'Sci1', 0, 48005, 48006 },
{'Sol1', 0, 48005, 48006 },
{'Sol2', 0, 48005, 48006 },
{'Sol3', 0, 48005, 48006 },
{'Sol4', 0, 48005, 48006 },
{'Sol5', 0, 48005, 48006 },
{'Eng1', 0, 48005, 48006 },
{'FEng1', 0, 48005, 48006 },
{'FMech1', 0, 48005, 48006 },
{'Off', 0, 48005, 48006 },
{'Ar1', 0, 48005, 48006 },
{'Ar2', 0, 48005, 48006 },
{'ArSol1', 0, 48005, 48006 },
{'', 0, 48005, 48006 },
{'ArSol2', 0, 48005, 48006 },
{'ASci1', 0, 48005, 48006 },
{'ASol1', 0, 48005, 48006 },
{'FAr1', 0, 48005, 48006 },
{'FMerc2', 0, 48005, 48006 },
{'Friend', 0, 48005, 48006 },
{'FRus1', 0, 48005, 48006 },
{'FSci1', 0, 48005, 48006 },
{'Merc1', 0, 48005, 48006 },
{'All', 0, 48005, 48006 },
{'', 0, 48005, 48006 },
{'Off1', 0, 48005, 48006 },
{'Off2', 0, 48005, 48006 },
{'RFSol1', 0, 48005, 48006 },
{'RSci1', 0, 48005, 48006 },
{'RSol1', 0, 48005, 48006 },
{'RSol2', 0, 48005, 48006 },
{'Rus1', 0, 48005, 48006 },
{'Rus2', 0, 48005, 48006 },
{'Rus3', 0, 48005, 48006 },
{'Russ', 0, 48005, 48006 },
{'FSol1', 0, 48005, 48006 },
{'Fsol2', 0, 48005, 48006 },
{'FSol3', 0, 48005, 48006 },
{'', 0, 48005, 48006 },
{'Fsol4', 0, 48005, 48006 },
{'Mech1', 0, 0, 0 }

};


function LoadLinesCustom(Character)
    CleanLines()
    local start = playDialogs.all[Character][3]
    local finish = playDialogs.all[Character][4]
    local characterIndex = playDialogs.all[Character][2]
    for i = start, finish do 
        local lineIndex = i - start + 1
        local lineText = loc(i)
        playDialogs.add(lineText, 1, characterIndex, lineIndex)
    end
end

function CleanLines()
  local count = playDialogs.count
  for x = count, 114, -1 do
    count = count - 1
    sgui_delete(playDialogs.list[x].ID)
  end
  playDialogs.count = count
end


spy=-12.5;
function TidyLines()
for x = 1,113 do spy = spy+15; setY(playDialogs.list[x],spy); end;
for a = 1,112 do setTexture(playDialogs.portraits[a],'/SGUI/Nao/heroes/'..playDialogs.heroes.names[a+1]..'.png'); end;
--setTexture(playDialogs.portraits[69],'/SGUI/Nao/heroes/Marshal Yashin.png');
end;

TidyLines();


-- [[ SEARCH WITHIN TEXTS FEATURE ]]

SearchEditBox 	    = getEditEX(nil,anchorNone,XYWH(805,75,125,25),CourierNew_12B,'','',COLOURS_DIALOG_EDIT,{callback_keypress='AutoSuggest(getText(SearchEditBox)); if (%k == VK_RETURN) then Search(getText(SearchEditBox)); end;'});
SearchEditBox.debog = getLabelEX(nil,anchorNone,XYWH(600,25,0,25),Tahoma_13,'Waiting...',{highlight=true, colour1=BLACKA(0), text_halign=ALIGN_LEFT, shadowtext=true, automaxwidth=195, autosize=true, wordwrap=true});

function Search(TEXT)
  for i = 1,113 do
    if getText(playDialogs.list[i]) == TEXT then 
	  setText(SearchEditBox,'found!'); 
	  clearFocus(); 
	  set_Colour(playDialogs.list[i].ID,PROP_FONT_COL,RGB(255,255,153));
	  SGUI_scrollbar_scroll(playDialogs.heroes.scrollbox.ID,-99999);
	  AddSingleUseTimer(2,'ScrollTo('..i..');');
	end;
  end;
end;

function ScrollTo(ID)
  local scroll = 0;
  repeat
    scroll = scroll+1;
	AddSingleUseTimer(scroll/10,'SGUI_scrollbar_scroll(playDialogs.heroes.scrollbox.ID,-70);');
  until scroll == ID-8;
  LoadLinesCustom(ID+1);
  AddSingleUseTimer(10,'set_Colour(playDialogs.list['..ID..'].ID,PROP_FONT_COL,WHITE());');
end;
	
auto_results = {};
function AutoSuggest_values()
  for i = 1,#playDialogs.all do 
    auto_results[i] = string.sub(playDialogs.all[i][1],1,string.len(playDialogs.all[i][1])-1);
  end;
end;

AutoSuggest_values();

function AutoSuggest(TEXT)
  local length = string.len(TEXT);
    if length > 2 then
      setText(SearchEditBox.debog,'Text: '..TEXT..'\nLength: '..length);
        for i = 1,#auto_results do 
          if string.sub(TEXT,1,length) == string.sub(auto_results[i],1,length) then 
	        setText(SearchEditBox.debog,getText(SearchEditBox.debog)..'\nAuto result: '..auto_results[i]..'\nLoc: '..loc(i+48999));
	        setText(SearchEditBox,auto_results[i]); 
	      end;
        end;
	end;
end;

