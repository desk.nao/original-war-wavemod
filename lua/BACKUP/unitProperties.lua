



for i = 1,52 do Constants.exclamations[i] = {"Exclamation n°"..i,i}; end;

function createOptionsWM(ID,PA,CA)
  labels = { 
    --[[Units    ]] { UNITS[12], DIRECTIONS, UNITS[2], UNITS[3], UNITS[4], UNITS[5], UNITS[6], UNITS[7], UNITS[8], HEALTH_POINTS, BOOLEANS, EXCLAMATIONS },

    --[[Vehicles ]] { UNITS[12], DIRECTIONS, HEALTH_POINTS, FUEL, CARGO, FLAG, MISCV }, 
	
    --[[Buildings]] { UNITS[12], HEALTH_POINTS, LEVELS, NAME, MISCB }
  };
  
  local callbacks = {
  {'511','516','510','soldier','engineer','mechanic','scientistic','stamina','speed','513','522','523'},
  {'511','516','513','524','525','526','527'},
  {'511','513','528','NAME','527b'}
  }; 

  local parent = menu[12];
  
  sgui_deletechildren(parent.ID);
  
  for i = 1,#labels[CA][ID] do 
    parent[i] = getLabelEX(parent,anchorNone,XYWH(5,2.5+((i-1)*15),171,15),Tahoma_13,labels[CA][ID][i][1],{text_halign=ALIGN_LEFT, callback_mouseclick=return_command(callbacks[CA][ID],i), automaxwidth=190-12, autosize=true, wordwrap=true, highlight=true});
	
	if labels[CA][ID] == UNITS[12] then set_Colour(parent[i].ID,PROP_FONT_COL,COLOURS[i]); end;
  end;

end;

function return_command(CASE,ID)
  if CASE == '511' or CASE == '516' then 
    return 'OW_CUSTOM_COMMAND('..CASE..','..(ID-1)..');'
  elseif CASE == '510' then 
    return 'OW_CUSTOM_COMMAND('..CASE..','..(UNITS[2][ID][2])..');'
  elseif CASE == 'soldier' then 
    return 'OW_CUSTOM_COMMAND(512,'..(1)..','..UNITS[3][ID][2]..');'
  elseif CASE == 'engineer' then 
    return 'OW_CUSTOM_COMMAND(512,'..(2)..','..UNITS[3][ID][2]..');'
  elseif CASE == 'mechanic' then 
    return 'OW_CUSTOM_COMMAND(512,'..(3)..','..UNITS[3][ID][2]..');'
  elseif CASE == 'scientistic' then 
    return 'OW_CUSTOM_COMMAND(512,'..(4)..','..UNITS[3][ID][2]..');'
  elseif CASE == 'stamina' then 
    return 'OW_CUSTOM_COMMAND(521,'..(1)..','..UNITS[7][ID][2]..');'
  elseif CASE == 'speed' then 
    return 'OW_CUSTOM_COMMAND(521,'..(2)..','..UNITS[8][ID][2]..');'
  elseif CASE == '513' then 
    return 'OW_CUSTOM_COMMAND('..CASE..','..HEALTH_POINTS[ID][2]..');'
  elseif CASE == '522' then 
    return 'OW_CUSTOM_COMMAND('..CASE..','..BOOLEANS[ID][2]..');'
  elseif CASE == '523' then 
    return 'OW_CUSTOM_COMMAND('..CASE..','..EXCLAMATIONS[ID][2]..');'
  elseif CASE == '524' then 
    return 'OW_CUSTOM_COMMAND('..CASE..','..FUEL[ID][2]..');'
  elseif CASE == '525' then 
    return 'OW_CUSTOM_COMMAND('..CASE..','..CARGO[ID][2]..');'
  elseif CASE == '526' then 
    return 'OW_CUSTOM_COMMAND('..CASE..','..FLAG[ID][2]..');'
  elseif CASE == '527' then 
    return 'OW_CUSTOM_COMMAND('..CASE..','..MISCV[ID][2]..');'
  elseif CASE == '528' then 
    return 'OW_CUSTOM_COMMAND('..CASE..','..LEVELS[ID][2]..');'
  elseif CASE == 'NAME' then 
    return 'OW_CUSTOM_COMMAND(0);'
  elseif CASE == '527b' then 
    return 'OW_CUSTOM_COMMAND('..(527)..','..MISCB[ID][2]..');'
  end;
end;