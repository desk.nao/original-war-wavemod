LOC_BOBBY = 49001;
LOC_JEFF = 49002;
LOC_CONNIE = 49003;
LOC_ANDY = 49004;
LOC_CYRUS = 49005;
LOC_PAUL = 49006;
LOC_DENIS = 49007;
LOC_LUCY = 49008;
LOC_FRANK = 49009;
LOC_GARY = 49010;
LOC_TIM = 49011;
LOC_RON = 49012;
LOC_HUGH = 49013;
LOC_JOHN = 49014;
LOC_JOAN = 49015;
LOC_LISA = 49016;
LOC_ARTHUR = 49017;
LOC_PETER = 49018;
LOC_JEREMY = 49019;
LOC_CATHY = 49020;
LOC_PETER = 49021;
LOC_YAMOKO = 49022;

LOC_ABDUL = 49023;
LOC_ROLF = 49024;
LOC_DIETRICH = 49025;
LOC_ROBERT = 49026;
LOC_HEIKE = 49027;
LOC_KURT = 49028;
LOC_OMAR = 49029;
LOC_RAOUL = 49030;

LOC_BELKOV1 = 49031;
LOC_BELKOV2 = 49032;
LOC_BORODIN = 49033;
LOC_BURLAK = 49034;
LOC_BYSTROV = 49035;
LOC_PYOTR = 49036;
LOC_LAZAR = 49037;
LOC_EISENSTEIN = 49038;
LOC_KONSTANTIN = 49039;
LOC_FYODOR = 49040;
LOC_GAYDAR = 49041;
LOC_GLADKOV = 49042;
LOC_GLEB = 49043;
LOC_GNYEVKO = 49044;
LOC_GOSSUDAROV = 49045;
LOC_GRISHKO = 49046;
LOC_KAPITSOVA = 49047;
LOC_KARAMAZOV = 49048;
LOC_KIRILENKOVA = 49049;
LOC_KOVALYUK = 49050;
LOC_KOZLOV = 49051;
LOC_KURIN = 49052;
LOC_KUZMOV = 49053;
LOC_LIPSHCHIN = 49054;
LOC_BEREZOV = 49055;
LOC_MOROZOV = 49056;
LOC_OBLUKOV = 49057;
LOC_PETROSYAN = 49058;
LOC_PETROVOVA = 49059;
LOC_PLATONOV = 49060;
LOC_POKRYSHKIN = 49061;
LOC_POPOV = 49062;
LOC_SCHOLTZE = 49063;
LOC_STOLYPIN = 49064;
LOC_TITOV = 49065;
LOC_TSARYTSYN = 49066;
LOC_GORKY = 49067;
LOC_YASHIN = 49068;

TID_Veh_Nothing 											 = 47000;
-- Engine
TID_engine_Combustion                                        = 47001;
TID_engine_Solar                                             = 47002;
TID_engine_Siberite                                          = 47003;
-- Control
TID_control_Manual                                           = 47101;
TID_control_Remote                                           = 47102;
TID_control_Computer                                         = 47103;
TID_control_Rider                                            = 47104;
TID_control_Apeman                                           = 47105;

-- American
	-- Chassis
TID_Veh_us_Light_wheeled                                     = 47201;
TID_Veh_us_Medium_wheeled                                    = 47202;
TID_Veh_us_Medium_tracked                                    = 47203;
TID_Veh_us_Heavy_tracked                                     = 47204;
TID_Veh_us_Morphling                                         = 47205;
	-- Weapon
TID_Weap_us_Machine_gun                                      = 47302;
TID_Weap_us_Light_gun                                        = 47303;
TID_Weap_us_Gatling_gun                                      = 47304;
TID_Weap_us_Double_gun                                       = 47305;
TID_Weap_us_Heavy_gun                                        = 47306;
TID_Weap_us_Rocket_launcher                                  = 47307;
TID_Weap_us_Siberium_rocket                                  = 47308;
TID_Weap_us_Laser                                            = 47309;
TID_Weap_us_Double_laser                                     = 47310;
TID_Weap_us_Radar                                            = 47311;
TID_Weap_us_Cargo_bay                                        = 47312;
TID_Weap_us_Crane                                            = 47313;
TID_Weap_us_Bulldozer                                        = 47314;

-- Arabian
	-- Chassis
TID_Veh_ar_Hovercraft                                        = 47211;
TID_Veh_ar_Light_trike                                       = 47212;
TID_Veh_ar_Medium_trike                                      = 47213;
TID_Veh_ar_Halftracked                                       = 47214;
TID_Veh_ar_Desert_rider                                      = 47215;
	-- Weapon
TID_Weap_ar_Multimissile_ballista                            = 47322;
TID_Weap_ar_Light_gun                                        = 47323;
TID_Weap_ar_Double_machine_gun                               = 47324;
TID_Weap_ar_Gatling_gun                                      = 47325;
TID_Weap_ar_Flamethrower                                     = 47326;
TID_Weap_ar_Gun                                              = 47327;
TID_Weap_ar_Rocket_launcher                                  = 47328;
TID_Weap_ar_Siberium_bomb                                    = 47363;
TID_Weap_ar_Selfpropelled_bomb                               = 47329;
TID_Weap_ar_Radar                                            = 47330;
TID_Weap_ar_Control_tower                                    = 47431;
TID_Weap_ar_Cargo_bay                                        = 47332;
TID_Weap_ar_Crane                                            = 47369;

-- Russian
	-- Chassis
TID_Veh_ru_Medium_wheeled                                    = 47221;
TID_Veh_ru_Medium_tracked                                    = 47222;
TID_Veh_ru_Heavy_wheeled                                     = 47223;
TID_Veh_ru_Heavy_tracked                                     = 47224;
TID_Veh_ru_Behemoth                                          = 47225;
TID_Veh_ru_Behemoth_Custom									 = 47226;
	-- Weapon
TID_Weap_ru_Heavy_machine_gun                                = 47342;
TID_Weap_ru_Gatling_gun                                      = 47343;
TID_Weap_ru_Gun                                              = 47344;
TID_Weap_ru_Rocket_launcher                                  = 47345;
TID_Weap_ru_Heavy_gun                                        = 47346;
TID_Weap_ru_Rocket                                           = 47347;
TID_Weap_ru_Siberium_rocket                                  = 47348;
TID_Weap_ru_Time_lapser                                      = 47349;
TID_Weap_ru_Radar                                            = 47350;
TID_Weap_ru_Cargo_bay                                        = 47351;
TID_Weap_ru_Crane                                            = 47352;
TID_Weap_ru_Bulldozer                                        = 47353;

TID_Weap_ru_Beh1                                             = 47356;
TID_Weap_ru_Beh2                                             = 47357;
TID_Weap_ru_Beh3                                             = 47358;
TID_Weap_ru_Beh4                                             = 47359;
TID_Weap_Mast_head                                           = 47361;

-- Nature
	-- Chassis
TID_Veh_na_Mastodon                                          = 47331;