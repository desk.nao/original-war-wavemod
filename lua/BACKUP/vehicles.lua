  for i = 1,6 do 
    local labels = {'Chassis','Engine','Weapon','Control','Nation','Side'};
    commands[9][i] = getCheckBoxEX_UI(dynamic.commands[23],anchorLT,XYWH(5,2.5+15*(i-1),15,15),labels[i],{},'',{checked=false,callback_mouseclick="VEHICLES:interface("..i..");"});
	set_Callback(commands[9][i].label.ID,CALLBACK_MOUSECLICK,sgui_getcallback(commands[9][i].ID,CALLBACK_MOUSECLICK));
  end;
  
commands[15] = 
shortButton(5,27.5+25,60,'TP',WHITE(),"TELEPORT()","label")

function TELEPORT()
  set_Callback(gamewindow.ID,CALLBACK_MOUSEMOVE,'Render(%x,%y);');
  ChangeTextureID('SGUI/Nao/hexagon',1)
  set_Callback(gamewindow.ID,CALLBACK_MOUSECLICK,'TPSUPP(%b,%x,%y);'); 
end


function TPSUPP(CASE,X,Y)
  switch(CASE) : caseof
  {
    [1]	= function (x)
	  set_Callback(gamewindow.ID,CALLBACK_MOUSECLICK,'');
	  set_Callback(gamewindow.ID,CALLBACK_MOUSEMOVE,'');
	  setVisible(MOUSEOVER,false);
	end,
    [0]	= function (x)
	  FH = OW_FINDHEX(X, Y)
	  H = OW_HEXTOSCREEN(FH.X, FH.Y)
	  if FH.FOUND == true then
		HXS = {FH.X, FH.Y};
		OW_CUSTOM_COMMAND(5482, HXS[1], HXS[2])
		TPSUPP(1,0,0)
	  end;
	end,	
  }
end