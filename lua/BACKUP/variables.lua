function SwitchInt()
  dynamic.id = dynamic.id+1;
  if dynamic.id > 2 then dynamic.id = 1; end;
  
  for i = 1,4 do 
    --setVisible(dynamic.sh[1][i],false); 
    --setVisible(dynamic.sh[2][i],false); 
  end;
  
  --for i = 1,#dynamic.sh[dynamic.id] do setVisible(dynamic.sh[dynamic.id][i],true); end;
end;



Switcher = getImageButtonEX(nil,anchorNone,XYWH(ScrWidth-220,50,100,25),'Switch','',"SwitchInt();",SKINTYPE_BUTTON,{font_colour_disabled=GRAY(127),});

DoInterfaceChange(interface.current);

Weapons = {
TID_Weap_us_Machine_gun,TID_Weap_us_Light_gun,TID_Weap_us_Gatling_gun,TID_Weap_us_Double_gun,TID_Weap_us_Heavy_gun,TID_Weap_us_Rocket_launcher,TID_Weap_us_Siberium_rocket,TID_Weap_us_Laser,TID_Weap_us_Double_laser,TID_Weap_us_Radar,TID_Weap_us_Cargo_bay,TID_Weap_us_Crane,TID_Weap_us_Bulldozer,

TID_Weap_ar_Multimissile_ballista,TID_Weap_ar_Light_gun,TID_Weap_ar_Double_machine_gun,TID_Weap_ar_Gatling_gun,TID_Weap_ar_Flamethrower,TID_Weap_ar_Gun,TID_Weap_ar_Rocket_launcher,TID_Weap_ar_Siberium_bomb,TID_Weap_ar_Selfpropelled_bomb,TID_Weap_ar_Radar,TID_Weap_ar_Control_tower,TID_Weap_ar_Cargo_bay,TID_Weap_ar_Crane,

TID_Weap_ru_Heavy_machine_gun,TID_Weap_ru_Gatling_gun,TID_Weap_ru_Gun,TID_Weap_ru_Rocket_launcher,TID_Weap_ru_Heavy_gun,TID_Weap_ru_Rocket,TID_Weap_ru_Siberium_rocket,TID_Weap_ru_Time_lapser,TID_Weap_ru_Radar,TID_Weap_ru_Cargo_bay,TID_Weap_ru_Crane,TID_Weap_ru_Bulldozer,

TID_Weap_ru_Beh1,TID_Weap_ru_Beh2,TID_Weap_ru_Beh3,TID_Weap_ru_Beh4,

TID_Veh_Nothing};

Engines = {
TID_engine_Combustion,TID_engine_Solar,TID_engine_Siberite};

Controls = {
TID_control_Manual,TID_control_Remote,TID_control_Computer,TID_control_Apeman};

Chassis = {
TID_Veh_us_Light_wheeled,TID_Veh_us_Medium_wheeled,TID_Veh_us_Medium_tracked,TID_Veh_us_Heavy_tracked,TID_Veh_us_Morphling,

TID_Veh_ar_Hovercraft,TID_Veh_ar_Light_trike,TID_Veh_ar_Medium_trike,TID_Veh_ar_Halftracked,TID_Veh_ar_Desert_rider,

TID_Veh_ru_Medium_wheeled,TID_Veh_ru_Medium_tracked,TID_Veh_ru_Heavy_wheeled,TID_Veh_ru_Heavy_tracked,TID_Veh_ru_Behemoth,TID_Veh_ru_Behemoth_Custom};

-- units parts
UNITS = {
--[[Nations]] { {'Nature',0},{'American',1},{'Arabian',2},{'Russian',3} },

--[[Classes]] { {'Soldier',1},{'Engineer',2},{'Mechanic',3},{'Scientist',4},{'Sniper',5},{'Mortar',8},{'Bazooker',9},{'Desert Warrior',11},{'Apeman',12},{'Apeman Soldier',15},{'Apeman Engineer',16},{'Apeman Kamikaze',17} },


--[[Military]] { {'Level: 0/10',0},{'Level: 1/10',1},{'Level: 2/10',2},{'Level: 3/10',3},{'Level: 4/10',4},{'Level: 5/10',5},{'Level: 6/10',6},{'Level: 7/10',7},{'Level: 8/10',8},{'Level: 9/10',9},{'Level: 10/10',10} },

--[[Engineering]] { {'Level: 0/10',0},{'Level: 1/10',1},{'Level: 2/10',2},{'Level: 3/10',3},{'Level: 4/10',4},{'Level: 5/10',5},{'Level: 6/10',6},{'Level: 7/10',7},{'Level: 8/10',8},{'Level: 9/10',9},{'Level: 10/10',10} },

--[[Mechanical]] { {'Level: 0/10',0},{'Level: 1/10',1},{'Level: 2/10',2},{'Level: 3/10',3},{'Level: 4/10',4},{'Level: 5/10',5},{'Level: 6/10',6},{'Level: 7/10',7},{'Level: 8/10',8},{'Level: 9/10',9},{'Level: 10/10',10} },

--[[Scientific]] { {'Level: 0/10',0},{'Level: 1/10',1},{'Level: 2/10',2},{'Level: 3/10',3},{'Level: 4/10',4},{'Level: 5/10',5},{'Level: 6/10',6},{'Level: 7/10',7},{'Level: 8/10',8},{'Level: 9/10',9},{'Level: 10/10',10} },





--[[Stamina]] { {"Stamina: "..(0),0},{"Stamina: "..(1),1},{"Stamina: "..(2),2},{"Stamina: "..(3),3},{"Stamina: "..(4),4},{"Stamina: "..(5),5},{"Stamina: "..(6),6},{"Stamina: "..(7),7},{"Stamina: "..(8),8},{"Stamina: "..(9),9},{"Stamina: "..(10), 10},{"Stamina: "..(11),11},{"Stamina: "..(12),12},{"Stamina: "..(13),13},{"Stamina: "..(14),14},{"Stamina: "..(15),15},{"Stamina: "..(16),16},{"Stamina: "..(17),17},{"Stamina: "..(18),18},{"Stamina: "..(19),19},{"Stamina: "..(20),20},{"Stamina: "..(21),21},{"Stamina: "..(22),22},{"Stamina: "..(23),23},{"Stamina: "..(24),24},{"Stamina: "..(25),25},{"Stamina: "..(26),26},{"Stamina: "..(27),27},{"Stamina: "..(28),28},{"Stamina: "..(29),29},{"Stamina: "..(30),30} },

--[[Speed]] { {"Speed: "..(1),1},{"Speed: "..(2),2},{"Speed: "..(3),3},{"Speed: "..(4),4},{"Speed: "..(5),5},{"Speed: "..(6),6},{"Speed: "..(7),7},{"Speed: "..(8),8},{"Speed: "..(9),9},{"Speed: "..(10),10},{"Speed: "..(11),11},{"Speed: "..(12),12},{"Speed: "..(13),13},{"Speed: "..(14),14},{"Speed: "..(15),15},{"Speed: "..(16),16},{"Speed: "..(17),17},{"Speed: "..(18),18},{"Speed: "..(19),19},{"Speed: "..(20),20},{"Speed: "..(21),21},{"Speed: "..(22),22},{"Speed: "..(23),23},{"Speed: "..(24),24},{"Speed: "..(25),25},{"Speed: "..(26),26},{"Speed: "..(27),27},{"Speed: "..(28),28},{"Speed: "..(29),29},{"Speed: "..(30),30} },

--[[Importance]] { {'Hero', 100}, {'Regular', 0} },
--[[Materialisation]] { {'No materialisation', 0}, {'Materalised unit', 1} },

--[[Gender]] { {'Male', 1}, {'Female', 2}, {'Random', 0} },

--[[Side]] { {'Neutral',0},{'Blue',1},{'Yellow',2},{'Red',3},{'Cyan',4},{'Orange',5},{'Purple',6},{'Green',7},{'Gray',8} }
};

