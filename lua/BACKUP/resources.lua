  commands[3]  = {--[[LAB1]] shortLabel(5,27.5+200+12.5,0,'Type of resources to send:',WHITE(),'Test!'),
				  --[[BUT1]] shortButton(5,27.5+182.5+12.5,getWidth(menu[1])-22,'Crates',WHITE(),"set_specialValue(3,1,%b);","Press this button to swap between deposit types (crates, oil, alaskite)."),
				  --[[LAB1]] shortLabel(5,27.5+150+12.5,0,'Amount of resources:',WHITE(),'Test!'),
				  --[[BUT1]] shortButton(5,27.5+132.5+12.5,getWidth(menu[1])-22,'Random amount',WHITE(),"set_specialValue(3,2,%b);","Press this button to specify the amount of resources to send per deposit."),
				  --[[LAB1]] shortLabel(5,27.5+100+12.5,0,'Materialisation effect?',WHITE(),'Test!'),
				  --[[BUT1]] shortButton(5,27.5+82.5+12.5,getWidth(menu[1])-22,'YES',WHITE(),"set_specialValue(3,3,%b);","Press this button to define whether or not the materialisation effect should play."),
				  --[[LAB1]] shortLabel(5,27.5+50+12.5,0,'Send anywhere?',WHITE(),'Test!'),
				  --[[BUT1]] shortButton(5,27.5+32.5+12.5,getWidth(menu[1])-22,'YES',WHITE(),"set_specialValue(3,4,%b);","Set this to YES if you'd like the resources to be sent anywhere on the map."),
  };