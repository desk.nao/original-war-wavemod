  commands[7] = {};
  
	local title = {'Blue','Yellow','Red','Cyan','Orange','Purple','Green','Gray'};
	for i = 1,8 do
	   commands[7][i] = shortLabel(5,275-25*(i-1),0,title[i]..':',Constants.colours[i+1],'');
	end
	for i = 1,8 do
	  commands[7][i+8] = shortButton(75,280-25*(i-1),getWidth(menu[1])-(127.5),'FOG '..i,WHITE(),"--[[CALLBACK]]","");
	  commands[7][i+8].VALUE = i;
	  sgui_setcallback(commands[7][i+8].ID,CALLBACK_MOUSECLICK,'FOG(commands[7]['..i..'+8],%b,'..i..');');
	end

  function FOG(ELEMENT,MOUSE,SIDE)
	  if MOUSE == 0 then
	    ELEMENT.VALUE = ELEMENT.VALUE+1;
		if ELEMENT.VALUE > 8 then ELEMENT.VALUE = 1 end
	    setText(ELEMENT,'FOG '..ELEMENT.VALUE);
	  else
	    ELEMENT.VALUE = ELEMENT.VALUE-1;
		if ELEMENT.VALUE < 1 then ELEMENT.VALUE = 8 end
	    setText(ELEMENT,'FOG '..ELEMENT.VALUE);
	  end
	  OW_CUSTOM_COMMAND(1015,SIDE,ELEMENT.VALUE);
  end