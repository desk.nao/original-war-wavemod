function units(TYPE)
  
  for i = 1,12 do setChecked(commands[8][i],false); end;
  setChecked(commands[8][TYPE],true);

  sgui_deletechildren(dynamic.commands[21].ID);

  for i = 1,#UNITS[TYPE] do
    dynamic.commands[21][i] = getCheckBoxEX_UI(dynamic.commands[21],anchorLT,XYWH(5,2.5+(i-1)*15,15,15),UNITS[TYPE][i][1],{},'',{checked=false,callback_mouseclick="for i = 1,"..#UNITS[TYPE].." do setChecked(dynamic.commands[21][i],false); end; setChecked(dynamic.commands[21]["..i.."],true); set_units("..TYPE..","..UNITS[TYPE][i][2]..","..i..");"});
	
	if TYPE == 2 and i > 8 then 
	  sgui_set(dynamic.commands[21][i].ID,PROP_HINT,'WARNING: Recent claims were made in our OW Discord server about the behaviours of apemen leading to sync loss issues. Place apemen on the map only if you are aware of this ongoing investigation!');
	  sgui_set(dynamic.commands[21][i].label.ID,PROP_HINT,'WARNING: Recent claims were made in our OW Discord server about the behaviours of apemen leading to sync loss issues. Place apemen on the map only if you are aware of this ongoing investigation!');
	end
	
	set_Callback(dynamic.commands[21][i].label.ID,CALLBACK_MOUSECLICK,sgui_getcallback(dynamic.commands[21][i].ID,CALLBACK_MOUSECLICK));
	if TYPE == 12 then set_Colour(dynamic.commands[21][i].label.ID,PROP_FONT_COL,Constants.colours[i]); end;
  end;
  
  setChecked(dynamic.commands[21][units_remember[TYPE]],true);

  DoInterfaceChange(interface.current);
end;

function set_units(TYPE,DATA,REMEMBER)
  units_remember[TYPE] = REMEMBER;
  units_data[TYPE] = DATA;
  setText(SearchEditBox.debog,units_data[TYPE]);
end;

VEHICLES = {}

function VEHICLES:interface(TYPE)
  local data = { Chassis, Engines, Weapons, Controls, UNITS[1], UNITS[12] };
  local text,textadd = '','';
  
  for i, checkbox in ipairs(commands[9]) do
    setChecked(checkbox, i == TYPE)
  end
  
  sgui_deletechildren(dynamic.commands[25].ID);



local type_data = data[TYPE]

for i = 1, #type_data do
  local textadd, text
  
  textadd = TYPE < 5 and string.sub(type_data[i], 4) or type_data[i][2]
  text = TYPE < 5 and loc(type_data[i])..' ('..textadd..')' or type_data[i][1]
	  
    dynamic.commands[25][i] = getCheckBoxEX_UI(dynamic.commands[25],anchorLT,XYWH(5,2.5+(i-1)*15,15,15),text,{},'',{checked=false,callback_mouseclick="for i = 1,"..#data[TYPE].." do setChecked(dynamic.commands[25][i],false); end; setChecked(dynamic.commands[25]["..i.."],true); VEHICLES:set_parts("..TYPE..","..textadd..","..i..");"});
	set_Callback(dynamic.commands[25][i].label.ID,CALLBACK_MOUSECLICK,sgui_getcallback(dynamic.commands[25][i].ID,CALLBACK_MOUSECLICK));
	
	if TYPE == 6 then set_Colour(dynamic.commands[25][i].label.ID,PROP_FONT_COL,Constants.colours[i]); end;

  end;

  if TYPE == 1 then 
    for i = 1,5 do set_Colour(dynamic.commands[25][i].label.ID,PROP_FONT_COL,Constants.colours[2]); end;
	for i = 6,10 do set_Colour(dynamic.commands[25][i].label.ID,PROP_FONT_COL,Constants.colours[3]); end;
	for i = 11,15 do set_Colour(dynamic.commands[25][i].label.ID,PROP_FONT_COL,Constants.colours[4]); end;
  end;
   
  if TYPE == 3 then 
    for i = 1,13 do set_Colour(dynamic.commands[25][i].label.ID,PROP_FONT_COL,Constants.colours[2]); end;
	for i = 14,26 do set_Colour(dynamic.commands[25][i].label.ID,PROP_FONT_COL,Constants.colours[3]); end;
	for i = 27,42 do set_Colour(dynamic.commands[25][i].label.ID,PROP_FONT_COL,Constants.colours[4]); end;
  end;
  
  setChecked(dynamic.commands[25][vehicle_remember[TYPE]],true);

  DoInterfaceChange(interface.current);
end;

function VEHICLES:set_parts(TYPE,COMPONENT,REMEMBER)
  vehicle_data[TYPE] = COMPONENT;
  vehicle_remember[TYPE] = REMEMBER;
  VEHICLES[TYPE] = COMPONENT;
end;

function VEHICLES:Create()
  local D = {}

  for i = 1,4 do 
    D[i] = string.format("%02d", vehicle_data[i])
  end

  local nations = {'u','a','r'}
  local nation = nations[vehicle_data[5]]

  ChangeTextureID('Faces/vehimg-'..nation..'-'..D[1]..'-'..D[2]..'-'..D[4]..'-'..D[3],0.5) 
  
  set_Callback(gamewindow.ID, CALLBACK_MOUSECLICK, [[
    if %b == 1 then 
      setVisible(MOUSEOVER,FALSE)
	  set_Callback(gamewindow.ID, CALLBACK_MOUSECLICK, '')
    elseif %b == 0 then 
      LOCAL.HEX:collect(getvalue(OWV_MYSIDE), %x, %y) 
      OW_CUSTOM_COMMAND(520,vehicle_data[5],vehicle_data[6]); OW_CUSTOM_COMMAND(518,vehicle_data[1],vehicle_data[2],vehicle_data[3],vehicle_data[4]); RunCommand(901);
    end
  ]])

end;