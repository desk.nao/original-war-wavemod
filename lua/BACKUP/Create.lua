function CreateUnit()
  local commands = {501,501,501,501,501,501,502,506}
  local args = {{2, units_data[1]},
                {4, units_data[2]},
                {5, units_data[9]},
                {8, units_data[10]},
                {1, units_data[12]},
                {6, units_data[11]},
                {units_data[3],units_data[4],units_data[5],units_data[6]},
                {units_data[7],units_data[8]}
               }
  
  for i, cmd in ipairs(commands) do
    OW_CUSTOM_COMMAND(cmd, unpack(args[i]))
  end
  
  ChangeTextureID('SGUI/Nao/hexagon',1)
  set_Callback(gamewindow.ID, CALLBACK_MOUSECLICK, [[
    if %b == 1 then 
      setVisible(MOUSEOVER,FALSE)
	  set_Callback(gamewindow.ID, CALLBACK_MOUSECLICK, '')
    elseif %b == 0 then 
      LOCAL.HEX:collect(getvalue(OWV_MYSIDE), %x, %y) 
      OW_CUSTOM_COMMAND(99, getvalue(OWV_MYSIDE)) 
      RunCommand(308) 
    end
  ]])
end

function CreateResources()
  ChangeTextureID('SGUI/Nao/hexagon',1)
  set_Callback(gamewindow.ID,CALLBACK_MOUSECLICK,[[
    if %b == 1 then 
      setVisible(MOUSEOVER,FALSE)
	  set_Callback(gamewindow.ID, CALLBACK_MOUSECLICK, '')
    elseif %b == 0 then 
      LOCAL.HEX:collect(getvalue(OWV_MYSIDE), %x, %y) 
	  OW_CUSTOM_COMMAND(507,special_value[3][1][1],special_value[3][2][1],special_value[3][3][1],special_value[3][4][1]);
	  RunCommand(301);
    end
  ]])
 
end;