-- VARIABLES
units_remember   = {1,9, 1,1,1,1,11,10,2,1,1,1};
units_data 	     = {0,12,0,0,0,0,10,10,0,0,1,0};
vehicle_remember = {1,1,1,1,1,1};
vehicle_data     = {1,1,2,1,1,1};
commands[8] 	 = {};
commands[9] 	 = {};
behaviour 		 = {0, {}, 0, --[[Y]]0 };
-- END OF VARIABLES

local folder = 'int/'
local files = {'vision','side','resources','properties','behaviors','fog','units','vehicles','buildings','objectives','queries','technologies'}

for _, file in ipairs(files) do
  include(folder .. file)
end

commands[210] = { {}, {}, {}, {}, {}, {}, {}, {} };


local X,Y = getX(menu[1]),getY(menu[1])
local W = getWidth(menu[1])
attitudes = getDialogEX(nil,anchorNone,XYWH(X-480,Y,475,220),SKINTYPE_DIALOG1,{visible=false,tile=true});

commands[5] = {};

for i = 3,10 do 
  attitudes[i] = {}
end

local function createButton(attitudes, row, col, action)
    attitudes[row][col] = getImageButtonEX(attitudes, anchorLT, XYWH(col*50, 22.5*(row-2), 25, 25), 'Neutral', '', 'attitudes:button('..row..','..col..')', SKINTYPE_BUTTON, {autosize = true})
end

for i = 1, 8 do 
    local row1_y = 27.5+(i-1)*(22.5)
    local row2_x = 52.5+(i-1)*(50)
    attitudes[1] = getLabelEX(attitudes, anchorNone, XYWH(5, row1_y, 50, 15), Tahoma_13, 'Side '..i, {font_colour = SIDE_COLOURS[i+1], text_halign = ALIGN_MIDDLE, callback_mouseclick = '', automaxwidth = 190-12, autosize = true, wordwrap = true, highlight = true, hint = 'hint'})
    attitudes[2] = getLabelEX(attitudes, anchorNone, XYWH(row2_x, 5, 55, 15), Tahoma_13, 'Side '..i, {font_colour = SIDE_COLOURS[i+1], text_halign = ALIGN_MIDDLE, callback_mouseclick = '', automaxwidth = 190-12, autosize = true, wordwrap = true, highlight = true, hint = 'hint'})
    for j = 3, 10 do
        createButton(attitudes, j, i, j + 2)
    end
end

  
function attitudes:button(LINE, ID)
  local ELE = attitudes[LINE][ID]
  local REVERT = attitudes[ID+2][LINE-2]
  local newText, newColor
  local attitude
  
  if getText(ELE) == 'Neutral' then 
    newText = 'Allied'
    newColor = SIDE_COLOURS[8]
	attitude = 1
  elseif getText(ELE) == 'Allied' then 
    newText = 'Ennemy'
    newColor = SIDE_COLOURS[4]
	attitude = 2
  else 
    newText = 'Neutral'
    newColor = SIDE_COLOURS[3]
	attitude = 0
  end

  setText(ELE, newText)
  set_Colour(ELE.ID, PROP_FONT_COL, RGB(0,0,0))
  set_Colour(ELE.ID, PROP_FONT_COL_BACK, newColor)
  
  setText(REVERT, newText)
  set_Colour(REVERT.ID, PROP_FONT_COL, RGB(0,0,0))
  set_Colour(REVERT.ID, PROP_FONT_COL_BACK, newColor)
  
  OW_CUSTOM_COMMAND(475,LINE-2,ID,attitude)
end
  
  DoInterfaceChange(interface.current);