commands[12] = {--[[EDI1]] shortEdit(5,27.5+25,getWidth(menu[1])-22,"Objective",WHITE(),"Type in an objective.")};
  
  for i = 2,5 do 
  local title = {'Delete objective','Strike objective','Outline objective','Colour objective'};
  local desc  = {'Removes an objective.','Strikes an objective.','Outlines an objective.','Applies a colour to an objective.'};
    commands[12][i] = shortButton(5,27.5+i*25,getWidth(menu[1])-22,title[i-1],WHITE(),"--[[CALLBACK]]",desc[i-1]);
  end