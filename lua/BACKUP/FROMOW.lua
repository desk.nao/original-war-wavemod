	  xicht = { 
	    {xicht_human, dynamic.commands[7],  dynamic.commands[9],  dynamic.commands[8],  dynamic.commands[10]}, 
	    {xicht_vehic, dynamic.commands[15], dynamic.commands[16], dynamic.commands[17], dynamic.commands[18]}, 
	    {xicht_build, dynamic.commands[11], dynamic.commands[12], dynamic.commands[13], dynamic.commands[14]} 
	  };
	  
function FROMOW_INFOPANEL_UPDATE(DATA) -- Called By OW! Typically when selecting any unit
	game_info.Update(DATA);
	
	if DATA.ID > 0 then

	  --setText(SearchEditBox.debog,serializeTable(DATA,"FROMOW_INFOPANEL_UPDATE"));
		
	  -- Name & Portrait
	  setText(commands[4][1],DATA.INFO[1]); 
	  set_Colour(commands[4][1].ID,PROP_FONT_COL,Constants.colours[DATA.SIDE+1]);
	  
	  if selected_id == 4 then
	    for i = 1,#xicht do 
		  if i ~= DATA.TYP then
		    for y = 1,#xicht[i] do 
		      setVisible(xicht[i][y],false); 
		    end;
		  else 
		    for y = 1,#xicht[i] do 
		      setVisible(xicht[i][y],true); 
		    end;
		  end;
	    end;
	  end;
	  
	  if selected_id == 15 then
	    for i = 1,3 do 
		  if i ~= DATA.TYP then
 
		      setVisible(xicht[i][1],false); 

		  else 

		      setVisible(xicht[i][1],true); 

		  end;
	    end;
	  end;
	  
	  SGUI_settextureid(xicht[DATA.TYP][1].ID,DATA.TEXTURE);
      
	  if DATA.TYP == 1 then -- Humans
	  
	    if DATA.SIDE == 0 and ischoosing == true then 
		  pass = true;
		  
		  if #behaviour[2] > 0 then 
		    for i = 1,#behaviour[2] do
		      if behaviour[2][i].value ~= nil and behaviour[2][i].value == DATA.ID then 
			    pass = false; 
			  end; 
		    end;
		  end;
		  
		  if pass == true then 
			addBehaviour(DATA.ID,DATA.TEXTURE,DATA.INFO[1]);
		  end;
			
		end;

		for i = 1,4 do -- SKILLS
		  SKILLS[i][2] = DATA.SKILLS[i];
		  SKILLS[i][1] = DATA.SKILLSTEXT[i][1].TEXT;
		  if dynamic.commands[9]["SKILLS"][i] ~= nil then setText(dynamic.commands[9]["SKILLS"][i],SKILLS[i][1]..': '..SKILLS[i][2]); end;
		end;

		for i = 1,2 do -- ATTRIBUTES
		  ATTRIBUTES[i][1] = DATA.VALUESTEXT[i][1].TEXT;
		  ATTRIBUTES[i][2] = DATA.VALUESTEXT[i][1].VALUE;
		  if dynamic.commands[9]["ATTRIBUTES"][i] ~= nil then setText(dynamic.commands[9]["ATTRIBUTES"][i],ATTRIBUTES[i][1]..': '..ATTRIBUTES[i][2]); end;
		end;

	  end;

	  if DATA.TYP == 2 then -- Vehicles
		
	  end;
	  
	  if DATA.TYP == 3 then -- Buildings
		
	  end;
		
		--special_value[4][1] = DATA.CLASS;
		--setText(commands[4][1],CLASSES[DATA.CLASS]); 
		
		--SGUI_settextureid(game.xicht.ID,DATA.TEXTURE); SGUI_settextureid(xicht_human.ID,DATA.TEXTURE);
		--AddSingleUseTimer(0.01,'OW_XICHT_SET(xicht_human.ID,dialog.objectives.customtext.ID);');
		--AddSingleUseTimer(0.02,'OW_XICHT_SET(game.xicht.ID,dialog.objectives.customtext.ID);');

	  end;
	  
	  OW_CUSTOM_COMMAND(1010, DATA.ID);
end;

set_Callback(SearchEditBox.debog.ID,CALLBACK_MOUSECLICK,"setY(SearchEditBox.debog,getY(SearchEditBox.debog)-25);");

function FROMOW_MULTI_PLAYERINFO(DATA)
--[[
	COUNT		Integer
	DATA	[1..12] (COUNT)
		NAME		STRING
		SIDE		Integer
		PING		Integer
		COLOUR		Integer
		PLID		Integer
		NATION		Integer
		ISCOMP		BOOL
		ISSPEC		BOOL
		ISDEDI		BOOL
		TEAM		Integer
		TEAMPOS		Integer
		ALIVE		BOOL
		NORESPONCE	BOOL
		LOADED		BOOL
		ISSERVER    BOOL

--]]
--setText(SearchEditBox.debog,serializeTable(DATA,"FROMOW_MULTI_PLAYERINFO"));

	for i=1,12 do
		setVisible(game.pinginfo.labels[i],false);
	end;

	for i=1,DATA.COUNT do
		setText(game.pinginfo.labels[i],DATA.DATA[i].NAME..' '..DATA.DATA[i].PING);
		setVisible(game.pinginfo.labels[i],true);
	end;

	--setVisible(game.pinginfo,true);

	MULTI_PLAYERINFO_CURRENT = DATA.DATA;
	MULTI_PLAYERINFO_CURRENT_PLID = {};
	for i=1,DATA.COUNT do
		MULTI_PLAYERINFO_CURRENT_PLID[DATA.DATA[i].PLID] = DATA.DATA[i];
	end;
	MULTI_PLAYERINFO_CURRENT.COUNT = DATA.COUNT;
	if loading.State == true then
		for i=1,DATA.COUNT do
			if loadingPlayers[i] then
				if DATA.DATA[i].LOADED or DATA.DATA[i].ISCOMP then
					if loadingPlayers[i].LoadingBar then
						sgui_set(loadingPlayers[i].LoadingBar.ID,PROP_PROGRESS,100);
					end;
				end;
--					setColour1(loadingPlayers[i],SIDE_COLOURS[DATA.DATA[i].COLOUR+1]);
			end;
		end;
	end;
end;