dyEx           = getElementEX(nil,anchorLT,XYWH(0,0,400,ScrHeight),false,{colour1=BLACKA(200)});
dyEx.caption   = getLabelEX(dyEx,anchorLT,XYWH(5,5,0,0),Tahoma_13,'',{visible=false, automaxwidth=300, autosize=true, wordwrap=true,});
dyEx.obj       = getElementEX(dyEx,anchorLT,XYWH(0,0,0,0),false,{colour1=BLACKA()});
dyEx.obj.list  = {};
dyEx.obj.count = 0;

dialog.objectives.customtext = getLabelEX(dyEx,anchorLT,XYWH(5,5,0,0),Tahoma_13,'',{visible=false, automaxwidth=300, autosize=true, wordwrap=true,});

function game.chat.edit.onkeypress(key)
	
	if (key == 46) then
		OW_CUSTOM_COMMAND(97);		
		game.chat.close();
        return;
	end;
	
	if (key == 13) then		

    local chatText = getText(game.chat.edit)
	
      if string.sub(chatText, 1, 4) == 'sayr' then
	    local playerName = getText(game.ui.infopanel.inner.text[1])
	    local message = string.sub(chatText, string.find(chatText, 'sayr') + 5)
	    for i = 1, 5 do
	      OW_MULTI_INGAME_SENDCHATMSG('<')
	    end
	    OW_MULTI_INGAME_SENDCHATMSG(playerName .. ': - ' .. message)
	    OW_CUSTOM_COMMAND_SGUI(2, 0, InfoPanelLastData.SIDE)
	    OW_CUSTOM_COMMAND(3000, InfoPanelLastData.SIDE, 2, 0, 0)
	    game.chat.close()
	    setText(game.chat.edit, '')
      end
	  
      if string.sub(chatText, 1, 3) == 'say' and not string.sub(chatText, 1, 4) == 'sayr' then
	    local playerName = getText(game.ui.infopanel.inner.text[1])
	    local message = string.sub(chatText, string.find(chatText, 'say') + 4)
	    for i = 1, 5 do
	      OW_MULTI_INGAME_SENDCHATMSG('<')
	    end
	    OW_MULTI_INGAME_SENDCHATMSG(playerName .. ': - ' .. message)
	    OW_CUSTOM_COMMAND_SGUI(22, 0, InfoPanelLastData.SIDE)
	    OW_CUSTOM_COMMAND(30000, InfoPanelLastData.SIDE, 2, 0, 0)
	    game.chat.close()
	    setText(game.chat.edit, '')
      end
		
        game.chat.close();
        OW_MULTI_INGAME_SENDCHATMSG(getText(game.chat.edit));
        setText(game.chat.edit,'');
        return;
	end;
	
    if (key == 27) then
        game.chat.close();
        return;
    end; 
	
end;

