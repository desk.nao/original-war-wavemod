commands[10] = {};

local labels = {'Nation', 'Type', 'Direction', 'Side', 'Level', 'Health', 'Modules', 'Units inside'}

  for i = 1,8 do 
    commands[10][i] = getCheckBoxEX_UI(dynamic.commands[27],anchorLT,XYWH(5,2.5+15*(i-1),15,15),labels[i],{},'',{checked=false,callback_mouseclick="buildings("..i..");"});
	set_Callback(commands[10][i].label.ID,CALLBACK_MOUSECLICK,sgui_getcallback(commands[10][i].ID,CALLBACK_MOUSECLICK));
  end;

local bNat = {{'American',1},{'Arabian',2},{'Russian',3}};

local bType = {
{'Depot',				0},
{'Warehouse',			1},
{'Armoury',				4},
{'Barracks',			5},
{'Workshop',			2},
{'Factory',				3},
{'Laboratory',			6},
{'Oil Power Plant', 	26},
{'Solar Power Plant',	27},
{'Siberite Power Plant',28},
{'Oil Drilling Tower',	29},
{'Siberite Mine',		30},
{'Breastworks',			31},
{'Manual Turret',		32},
{'Automatic Turret',	33},
{'Control Tower',		36},
{'Russian Behemoth',	37},
{'Russian Teleport',	34},
{'EON/TAWAR',  	        38},
{'Alien building',  	39},
{'Russian Wall',		40}
};

local bDir = {
  {'Top right', 0},
  {'Right', 1},
  {'Bottom right', 2},
  {'Bottom left', 3},
  {'Left', 4},
  {'Top left', 5}
}

local bSide = {
  {'Neutral', 0},
  {'Blue', 1},
  {'Yellow', 2},
  {'Red', 3},
  {'Cyan', 4},
  {'Orange', 5},
  {'Purple', 6},
  {'Green', 7},
  {'Gray', 8}
}

local bLevel = {
  {'Level: 0/10', 0},
  {'Level: 1/10', 1},
  {'Level: 2/10', 2},
  {'Level: 3/10', 3},
  {'Level: 4/10', 4},
  {'Level: 5/10', 5},
  {'Level: 6/10', 6},
  {'Level: 7/10', 7},
  {'Level: 8/10', 8},
  {'Level: 9/10', 9},
  {'Level: 10/10', 10}
}

local bHealth = {
  {'Low', 250},
  {'Medium', 500},
  {'Perfect', 1000}
}

local bModules = {
  {'Soon', 1},
  {'Soon!', 1}
}

local bUnits = {
  {'Soon', 1},
  {'Soon!', 1}
}

local bData = {1, 1, 1, 1, 1, 3, 1, 1}

local file_name = {
'depot',
'warehouse',
'workshop',
'factory',
'armoury',
'barracks',
'basiclab',
'bridgeupg',
'bridgeupg',
'basiclab',
'weaponlab',
'siberiumlab',
'computerlab',
'biologicallab',
'spacetimelab',
'optolab',
'trackext',
'gunext',
'rocketext',
'non-combatext',
'radarext',
'siberiumext',
'radioext',
'solarext',
'computerext',
'laserext',
'oilpower',
'solarpower',
'siberiumpower',
'oilmine',
'siberiummine',
'breastworks',
'bunker',
'automaticturret',
'teleport',
'bunker-s',
'ct',
'behemoth',
'eon',
'alien',
'wall'};
  
local bType = {
{'Depot',				0},
{'Warehouse',			1},
{'Armoury',				4},
{'Barracks',			5},
{'Workshop',			2},
{'Factory',				3},
{'Laboratory',			6},
{'Oil Power Plant', 	26},
{'Solar Power Plant',	27},
{'Siberite Power Plant',28},
{'Oil Drilling Tower',	29},
{'Siberite Mine',		30},
{'Breastworks',			31},
{'Manual Turret',		32},
{'Automatic Turret',	33},
{'Control Tower',		36},
{'Russian Behemoth',	37},
{'Russian Teleport',	34},
{'EON/TAWAR',  	        38},
{'Alien building',  	39},
{'Russian Wall',		40}
};
  
nation = 'u'
REVERT = 1;
function buildings(TYPE)
  local data = { bNat, bType, bDir, bSide, bLevel, bHealth, bModules, bUnits };
  for i = 1, 8 do
    setChecked(commands[10][i], i == TYPE)
  end
  sgui_deletechildren(dynamic.commands[29].ID)
  
  local DATA = data[TYPE]
  local PARENT = dynamic.commands[29]
  

	for i = 1, #DATA do
	  local callback = nil;
	  local Y = 2.5 + (i - 1) * 15;
	
		switch(TYPE) : caseof {
		[1]	= function (x) -- Nation
				local callback = 'BUILDING_NATION('..i..');'
				PARENT[i] = getCheckBoxEX_UI(PARENT, anchorLT, XYWH(5, Y, 15, 15), DATA[i][1], {}, '', {checked = false, callback_mouseclick = callback})

				set_Callback(PARENT[i].label.ID, CALLBACK_MOUSECLICK, callback)
				set_Colour(PARENT[i].label.ID, PROP_FONT_COL, Constants.colours[i + 1])

			  end,
		[2]	= function (x) -- Type
			    local data = {'u','r','r','u','u','r'}
				local sub_nations = {'u','a','r'}
				nation = i > 15 and data[i-15] or sub_nations[REVERT]
				
				local callback = "for i = 1, " .. #DATA .. " do setChecked(dynamic.commands[29][i], false) end; setChecked(dynamic.commands[29][" .. i .. "], true); CALL_PREVIEW(" .. TYPE .. ", " .. i .. ")"
				PARENT[i] = getCheckBoxEX_UI(PARENT, anchorLT, XYWH(5, Y, 15, 15), DATA[i][1], {}, '', {checked = false, callback_mouseclick = callback})
				PARENT[i][1] = getElementEX(PARENT[i],anchorNone,XYWH(150,0,17.5,17.5),true,{scissors=true,texture='SGUI/buildings_small/b-'..nation..'-'..file_name[DATA[i][2]+1]..'0000.png',bNATION=nation});

				set_Callback(PARENT[i].label.ID, CALLBACK_MOUSECLICK, sgui_getcallback(PARENT[i].ID, CALLBACK_MOUSECLICK))

			  end,
		[4] = function (x) -- Side
				PARENT[i] = getCheckBoxEX_UI(PARENT, anchorLT, XYWH(5, Y, 15, 15), DATA[i][1], {}, '', {checked = false, callback_mouseclick = callback})
				set_Colour(PARENT[i].label.ID, PROP_FONT_COL, Constants.colours[i])
			end,
		};
	end
	
	AddSingleUseTimer(0.1,[[
      if ]]..TYPE..[[ == 1 then
	    setChecked(dynamic.commands[29][REVERT], true)
      end]])
  
  DoInterfaceChange(interface.current)

end

DIRECTION = 1;

function BUILDING_NATION(NATION)
  for i = 1, 3 do 
    setChecked(dynamic.commands[29][i], i == NATION) 
  end 
  
  local data = {'u', 'a', 'r'}
  nation = data[NATION]
  REVERT = NATION;
  setText(SearchEditBox.debog, 'nation: '..nation..' NATION: '..NATION)
end

function CALL_PREVIEW(CATEGORY,DATA)
  bData[CATEGORY] = DATA;
  
  local NAT = dynamic.commands[29][DATA][1].bNATION;
  if NAT == 'r' then unat = 3 end
  if NAT == 'u' then unat = 1 end
  if NAT == 'a' then unat = 2 end
  
  set_Callback(gamewindow.ID,CALLBACK_MOUSEMOVE,'Render(%x,%y);');
  set_Callback(gamewindow.ID, CALLBACK_MOUSECLICK,'LOCAL.HEX:collect('..getvalue(OWV_MYSIDE)..',%x,%y); EXECUTE_PREVIEW(%b,'..DATA..');'); 
 
  ChangeTextureID('SGUI/buildings/b-'..NAT..'-'..file_name[bType[DATA][2]+1]..'000'..(DIRECTION-1),1);

end

function EXECUTE_PREVIEW(BUTTON,DATA)
  local NAT = dynamic.commands[29][DATA][1].bNATION;
  if NAT == 'r' then unat = 3 end
  if NAT == 'u' then unat = 1 end
  if NAT == 'a' then unat = 2 end

  if BUTTON == 0 then 
	OW_CUSTOM_COMMAND(5188,bType[DATA][2],unat,DIRECTION-1);
	set_Callback(gamewindow.ID, CALLBACK_MOUSECLICK,'LOCAL.HEX:collect('..getvalue(OWV_MYSIDE)..',%x,%y);'); 
	setVisible(MOUSEOVER,false);
  elseif BUTTON == 1 then 
    DIRECTION = DIRECTION +1;
  end
  
  if DIRECTION > 6 then DIRECTION = 1; end
  
  PREVIEW = loadOGLTexture('SGUI/buildings/b-'..NAT..'-'..file_name[bType[DATA][2]+1]..'000'..(DIRECTION-1)..'.png',true,false,false,false);
  
end