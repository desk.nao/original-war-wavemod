BIO = {};

BIO.RANK = {
"General of the Army",
"General",
"Lieutenant General",
"Major General",
"Brigadier General",
"Colonel",
"Lieutenant Colonel",
"Major",
"Captain",
"First Lieutenant",
"Second Lieutenant",
"Sergeant Major",
"Sergeant",
"Corporal",
"Lance Corporal",
"Private"};

BIO.DEGREE = {
"Ph.D.",
"M.A.",
"B.Sc.",
"Dr.",
"M.D."
};

BIO.ACADEMY = {

["AL"] = { -- Albania
"Potytechnic University of Tirana"
},

["AM"] = {
"Abraham Baldwin Agricultural College, Georgia","Academy Canada","Acadia University","Algoma University College","American International College, Springfield, Massachusetts","American University","Antioch University, Seattle, Washington","Arkansas State University","Armstrong State University","Athabasca University","Bellarmine College, Kentucky","Berkeley College","Boston University","British Columbia Institute of Technology","CALTECH","Campbellsville University, Kentucky","Carnegie-Mellon University","Carthage College, Kenosha, Wisconsin","Central Michigan University","City University of New York","College Notre-Dame","Colorado State University","Columbia College","Columbia University","Columbus State University","Concordia University","Cornell University","Cumberland College","DeVry Institute of Technology","Ferris State University","Fort Lewis College","George Washington University","Georgia College","GMI Engineering & Management Institute","Grace University, Nebraska","Hampshire College","Harvard University","Hofstra University","Huron College","Chicago State University","Idaho State University","Indiana Institute of Technology","John Brown University","Kentucky State University","King's College","Knox College","LaGrange College, Georgia","Le Campus du Fort Saint-Jean","Long Island University","Madonna University, Michigan","Massachusetts Institute of Technology","McGill University","Morehouse College, Georgia","Mount Royal College","Murray State University","New York Institute of Technology","New York University","North Arkansas College","Oakland University","Our Lady of the Lake University","Philander Smith College, Little Rock, Arkansas","Pratt Institute","Purdue University Calumet","Roosevelt University","Reserve Officers' Training Corps","Saint Lawrence University","Salem State College","Seattle Pacific University","Seattle University","Silver Lake College","State University of New York College of Technology","State University of West Georgia","Syracuse University","Texas A&M International University","Texas Tech University","the United States Air Force Academy in Colorado","the United States Merchant Marine Academy at Kings Point","the United States Military Academy at West Point","the United States Naval Academy at Annapolis","the University of Southern California","Transylvania University, Lexington, Kentucky","Université du Québec","University of Colorado","University of Denver","University of Georgia","University of Houston","University of Idaho","University of Illinois","University of Kentucky","University of Louisville","University of Michigan","University of Nebraska","University of Notre Dame","University of Texas","University of Washington","University of Wisconsin","Viterbo College","Washington State University","Wayne State University","Wesleyan College, Georgia"
},

["AR"] = {
"Ajman University College of Science & Technology","Al al-Bayt University","Al-Imam Mohamed Ign Saud Islamic University","American University in Dubai","Amman University","Dubai Aviation College","Dubai Polytechnic","Etisalat College of Engineering","Hashemite University","Islamic University Medinah","King Abdul Aziz University","King Faisal University","King Saud University","Umm Al-Qura University","United Arab Emirates University","Yarmouk University"
},

["AU"] = {
"Australian Defence Force Academy","Avondale College","Queensland University of Technology","The Australian National University","The University of Adelaide","The University of Melbourne","University of Canberra","Donau-University Krems","Graz University of Technology","Johannes Kepler University of Linz","University of Salzburg","University of Vienna","Vienna University of Technology"
},

["BE"] = {
"Boston University Brussels","Faculté Polytechnique de Mons","Institut Superieur Industriel de Bruxelles","Université Libre de Bruxelles","Vrije Universiteit Brussel"
},

["CZ"] = {
"Charles University (Prague)","Masaryk University (Brno)"
},

["FR"] = {
"Ecole Nationale des Ponts et Chausees","Ecole Nationale d'Ingénieurs de Metz","Ecole Nationale d'Ingénieurs de Saint-Etienne","Ecole Nationale Supérieur de Mécaniques et des Microtechniques","Ecole Nationale Supérieure de Chimie et de Physique de Bordeaux","Ecole Nationale Supérieure de l'Aéronautique et de l'Espace","Ecole Nationale Supérieure de Physique de Strasbourg","Ecole Nationale Supérieure des Telecommunications de Paris","Ecole Nationale Superieure d'Informatique et de Mathematiques Appliquees de Grenoble","Ecole Normale Supérieure de Lyon","Ecole Normale Supérieure de Paris","Ecole Polytechnique","Ecole Supérieure d'Electricité","Ecole Universitaire d'Ingénieurs de Lille","Institut de Recherche et d'Enseignement Supérieur aux Techniques de l'électronique","Institut des Sciences de la Matière et du Rayonnement","Institut des Sciences de l'Ingénieur de Clermont-Ferrand","Institut des Sciences de l'Ingénieur de Montpellier","Institut National des Télécommunications","Institut National Polytechnique de Grenoble","The Special Military School of St. Cyr","Université Bordeaux I","Université de Caen Basse Normandie","Université de Nantes","Université de Picardie Jules-Verne","Université Sorbonne-Nouvelle (Paris III)"
},

["AN"] = { -- Andorra
"Estudis Virtuals d'Andorra","Lycée Comte de Foix Andorre"
},

["IS"] = { -- Iceland
"Technical College of Iceland","University College of Education, Reykjavík","University of Akureyri""Bar-Ilan University, Ramat Gan","Ben-Gurion University of the Negev","Hebrew University of Jerusalem","Tel Aviv University","University of Haifa"
},

["NL"] = { -- Netherlands
"Delft University of Technology","Eindhoven University of Technology","Erasmus University of Rotterdam","Free University Amsterdam","Haagse Hogeschool","Hanzehogeschool","Hogeschool Eindhoven","Hogeschool van Amsterdam","Hogeschool van Utrecht","IJselland Polytechnic","Leiden University","Noordelijke Hogeschool Leeuwarden","Tilburg University","Utrecht University"
},

["UG"] = { -- Uganda
"Makerere University","Uganda Martyr's University"
},

["RU"] = {
"Altai State Technical University","Altay State University","Arhangelsk State Technical University","Bashkir State University","Budker's Institute of Nuclear Physics","Dubna International University for Nature, Society and Man","Chelyabinsk Technical University","Chuvash State University","Institute of High Energy Physics","Institute of Microelectronics Technology and High Purity Materials","Institute of Solid State Physics","Irkutsk State University","Ivanovo State University of Power Engineering","Joint Institute for Nuclear Research","Kazan State University of Technology","Khabarovsk State Technical University","Krasnoyarsk State Technical University","Landau Institute for Theoretical Physics","Lebedev's Physical Institute","Leningrad Nuclear Physics Institute","Leningrad State Technical University","M.V. Lomonosov Moscow State University","Mendeleev Russian University of Chemical Technology","Moscow Institute of Physics and Technology","Moscow State Institute of International Relations","Nakhimov Naval Secondary School","Nizhny Novgorod State Technical University","Nizhny Novgorod State University","Novosibirsk State Technical University","Petrozavodsk State University","Plekhanov Russian Academy of Economics","People' s Friendship University of Russia","Rostov-on-Don State University","Samara State Technical University","Saratov State University","Siberian State Industrial University","State Technical University of Penza","Suvorov Military Schools","Tomsk Polytechnic University","Tomsk State University","Tyumen State University","Yakutsk State University","Yaroslavl State University"
},

["BU"] = { -- Bulgaria
"Plovdiv University","Shoumen University 'Konstantin Preslavski'","Technical University of Gabrovo"
},

["AG"] = { -- Angola
"Catholic University of Angola"
},

["SA"] = {
"Potchefstroom University for Christian Higher Education","Rand Afrikaans University","Rhodes University","Technikon Southern Africa","University of Cape Town","University of Durban-Westville","University of Natal (Durban)","University of Pretoria","University of South Africa","University of Stellenbosch","University of the Orange Free State","University of the Western Cape","University of Witwatersrand","Vista University"
},

["FI"] = {
"Abo Akademi University","Häme Polytechnic","Lappeenranta University of Technology","Oulu Institute of Tecnology","Tampere Institute of Technology"
},

["CA"] = {
"College Militaire Royale","Marine Institute St. John's"
},

["UK"] = {
"Bristol University","Cambridge University","City University","Dundee University","Durham University","Glasgow University","Imperial College of Science and Technology","King's College London","Loughborough University of Technology","Merton College, Oxford","Oxford University","Reading University","Sandhurst Royal Military Academy","St.Andrews University","University of Abertay Dundee","University of Edinburgh","University of Exeter","University of London","University of St. Andrews","University of York","Wolfson College, Oxford"
}

};

BIO.INSTITUTE = {

["AM"] = {"Jet Propulsion Laboratory, Pasadena","Lawrence Livermore National Laboratory","Los Alamos National laboratory","AT&T Bell Labs.","Cray Research, Inc.","Dow Chemical Co.","DuPont Corp.","Eastman Kodak Co.","IBM Corp.","Glaxo Research Institute","Pfizer Inc.","3M Corp.","BASF Bioresearch Corp.","Northrop Corp.","Mobil Oil Corp.","Goddard Institute for Space Studies, New York","Goddard Space Flight Center, Greenbelt","Kennedy Space Center, Florida","Yale University","Glaxo Wellcome Co.","Marion Merrell Dow Pharmaceuticals","Proctor and Gamble Co.","Schering-Plough Research","Langley Research Center, Hampton, Virginia","Los Alamos Technology Associates","Glenn Research Center, Cleveland","Moffett Federal Airfield, Mountain View, California","Ernest Orlando Lawrence Berkley National Laboratory","Princeton Plasma Physics Laboratory","Cornell University","Tektronix, Inc.","Ames Research Center, California","Dryden Flight Research Center, Edwards, California","Johnson Space Center, Houston","Marshall Space Flight Center, Huntsville, Alabama","Akzo Nobel N.V.","Creative Biomolecules, Inc.","Defense Advanced Research Projects Agency","Hewlett-Packard Co.","Honeywell, Inc.","Merck & Co.","University of California","University of California, San Diego","CIBA-Corning Diagnostics","Rutgers University","Stennis Space Center, Massachuttes","Wallops Flight Facility, Virginia","White Sands Test Facility, New Mexico","Knolls Atomic Power Laboratory","Advanced Imaging Systems, Inc.","Advanced Peptides Inc.","Agracetus, Inc.","Allied-Signal, Inc.","American Home Products Corp","American Inst. for Cancer Research","American National Red Cross","Amgen-Regeneron Partners","Amoco Technology Co.","American Radio and Research Corporation","Antex Biologics, Inc.","Applications Development Institute","Applied Biosystems Inc.","Applied Genetics Lab.","Applied Imaging, Inc.","Athena Neurosciences, Inc.","Atlantic Pharmaceuticals, Inc.","Atrium Medical Corp.","Auragen Inc.","Aurora Technologies, Inc.","Aviron Inc.","Baker Norton Pharmaceuticals Inc.","Bardwell Industries, Inc.","Baxter Labs.","Becton Dickinson & Co.","Bio-Brite, Inc.","BioCarb, Inc.","Biokit Labs.","Biomedical Research & Development Lab","Bio-Nucleonics, Inc.","BioSeiche Therapeutics","BioSource Genetics Corp.","Biospherics Inc.","BioTrax, Inc.","Boston Scientific Corp.","Boston University","Boyle Engineering, Inc.","Brandon Research, Inc.","Cadwell Labs., Inc.","Calcitek, Inc.","Cambridge Biotech Corp.","Carlson Technology, Inc.","Carrington Labs.","Carter-Wallace, Inc.","Case Western Reserve University","Catalytica Inc.","Cell Genesys Inc.","CellPro Inc.","Celox Laboratories, Inc.","Celtrix Pharmaceuticals","Cetus Corp.","Clarity, Inc.","Clorox Co., Inc.","Colla-Tec, Inc.","Colorado Advanced Technology Instititute","Colorado State University","CombiChem, Inc.","Connaught Labs.","Continental Colloids, Inc.","ConvaTec, Inc.","Corixa Corp.","Cytel Corp.","Cytonix Corp.","Daiichi Pharmaceutical Co., Ltd.","Darwin Molecular Corp.","Deaton Ashcraft Group, Inc.","Defense Advanced Research Projects","Dekker, Marcel, Inc.","Diatech Inc.","DiaXotics, Inc.","Digital Instruments, Inc.","Drexel University","Earl-Clay Labs.","Elan Corp.","Electric Power Research Inst.","Electromedics, Inc.","Eli Lilly & Co.","Embrex Inc.","Emory University","EntreMed Inc.","Enzymol International","Exponential Biotherapies Inc.","Fisher-Scientific Co.","Futrex Inc.","GalaGen, Inc.","GE Medical Systems","General Atomics","General Biometrics, Inc.","General Electric Co.","General Motors Corp.","GENE-TRAK Systems, Inc.","Glaxo Holdings PLC","Graseby-NuTech","Greer Laboratories, Inc.","Guilford Pharmaceuticals, Inc.","Gustafson, Inc.","Howard University","Chemicon International Inc.","Chiron Corp.","ICI Americas, Inc.","IG Labs.","Illinois Inst. of Technology","Imagenics, Inc.","Individual Monitoring Systems, Inc.","Innovir Labs.","IVAX Pharmaceuticals","Jenner Technologies, Inc.","John Long and Associates","Johns Hopkins University","Johnson & Johnson Co.","Kao Corp.","Kissei Pharmaceutical Co., Ltd.","Kyoto College of Pharmacy","LeCroy Research Systems, Inc.","Leica Imaging Systems, Ltd.","Louisiana State University","Maxim, Inc.","Mayo Clinic","MediChem Research, Inc.","Microbiological Associates, Inc.","Neogen Inc.","NeoRx Corp.","Neurogen Corp.","New Brunswick Scientific Co., Inc.","New Mexico State University","Nimbus Medical, Inc.","Nitrate Elimination Co., Inc.","Northeastern Louisiana University","Nova Pharmaceutical Corp.","Novagen, Inc.","Novartis","Ohio Advanced Technology Center","Ohio State University","Ohmicron Inc.","Ochsner, Alton Medical Foundation","OraVax, Inc.","Orbital Technologies Corp.","Organon Teknika Corp.","Oxford Biosciences Ltd.","Pall Corp.","PARC Institute Inc.","Parke-Davis Div., Warner-Larbert Co","Phoenix Laser Systems Inc.","Phyton Corp.","PROCOR Technologies, Inc.","Quantech, Ltd.","Quantex Corp.","Quatro Corp.","Ranpak Corp.","Regeneron Pharmaceuticals, Inc.","Rockefeller University","Rohm and Haas Co.","Ross Breeders, Inc.","Ross Labs.","Sabolich Inc .","SciClone Pharmaceuticals, Inc.","Siegle Inst.","Siemens Pacesetter, Inc.","SpaceLabs Medical, Inc.","Stanford University","State University of New York","Sterling Winthrop Inc.","Stryker Corp.","Syntex U.S.A Corp.","Tanabe Research Labs.","Teledyne Brown Engineering","Teledyne Microelectronics","Texas A&M University","TheraGuide Inc.","Thomson-CGR","Trinity University","TSI Corp.","University of Baltimore","University of Colorado","University of Iowa","University of Kentucky","University of Maryland","University of Michigan","University of Minnesota","University of New Mexico","University of Pennsylvania","University of Pittsburgh","University of Alabama","University of Alaska","University of Arizona","University of Arkansas","University of Cincinnati","University of Florida","University of Hawaii at Manoa","University of Houston","University of Kentucky","University of Louisville","University of Maryland","University of Massachusetts","University of Miami","University of Minnesota","University of New Orleans","University of North Carolina","University of Oulu","University of Pennsylvania","University of South Carolina","University of Texas","University of Virginia","University of Wisconsin","University Studi Padova","Vindicator, Inc.","Vitek, Inc.","Walter Reed Army Inst. of Research","Wang NMR Inc.","Warner-Lambert Co.","Weiner Labs.","Wyeth-Ayerst Labs","Zinatha Consortium","Zinpro Corp."},

["FR"] = {"Rhone-Poulenc S.A.","Pasteur Merieux S.A.","Guerbet S.A.","Institut Merieux","Rhone Merieux Laboratoire IFFA","Sanofi SA","Virbac S.A."},

["GE"] = {"Hoechst AG","CIBA-GEIGY AG","Boehringer Ingelheim International","Boehringer Ingelheim Intl. GmbH","Boehringer Mannheim Gmbh","Carl Zeiss Inc.","Hoechst Marion Roussel","Sandoz Forschungsinsitut","Schering AG","Beiersdorf AG","University of Berne","Bayer AG","Behringwerke AG","Sanofi Winthrop"},

["IT"] = {"Instituto Biologico"},

["IZ"] = {"Yeshiva University"},

["JP"] = {"Fujisawa Pharmaceuticals","Misui Engineering and Shipbuilding","Nikko Kyodo Co., Ltd.","Nippon Paper Industries Co., Ltd.","Sanyo-Kokusaka Pulp Co."},

["NL"] = {"Lederle-Praxis Biologicals","University of Guelph"},

["NO"] = {"Novo Nordisk AS"},

["RZ"] = {"University of Dar Es Salaam (Tanzania)","Zimbabwe National Traditional Healers Association"},

["RU"] = {"Joint Institute for Nuclear Research, Dubna","Institute of Nuclear Physics, Moscow","M. M. Gromov Flight Research Institute, Moscow","B.P. Konstantinov Nuclear Physics Institute, Leningrad","Chelyabinsk Nuclear Center","L. V. Kirenkski Physics Institute, Krasnoyarsk","G.I. Budker Institute of Nuclear Physics, Nobosibirsk"},

["FI"] = {"University of Tampere, Finland"},

["SP"] = {"Eniricerche SpA","Fidia SpA"},

["SW"] = {"Hafslund Nycomed AS","Swedish National Bacteriological Laboratory","Swenko R&D, Inc."},

["UK"] = {"SmithKline Beecham Corp.","CIBA-GEIGY Ltd.","Hoffmann-La Roche Inc.","Bristol Myers Squibb Co.","Amersham International PLC","BioNumerik Pharmaceuticals, Inc.","British Biotech Pharmaceuticals, Ltd"}

};

BIO.ARMY = {
["AL"] = {
"Black Swans"
},

["AM"] = {
"1st Armored Division","1st Cavalry Division","1st Brigade, 1st Cavalry Division","1st Infantry Division","2d Brigade, 1st Infantry Division","Engineer Brigade, 1st Infantry Division","2d Infantry Division","1st Brigade, 2d Infantry Division","2d Brigade, 2d Infantry Division","3d Brigade, 2d Infantry Division","3d Infantry Division","1st Brigade, 3d Infantry Division","Aviation Brigade, 3d Infantry Division","4th Infantry Division","1st Brigade, 4th Infantry Division","10th Mountain Division","25th Infantry Division","25th Infantry Division Artillery","35th Infantry Division","69th Brigade, 35th Infantry Division","149th Brigade, 35th Infantry Division","35th Infantry Division Artillery","82d Airborne Division","2d Brigade, 82d Airborne Division","82d Airborne Division Artillery","101st Airborne Division","1st Brigade, 101st Airborne Division","2d Brigade, 101st Airborne Division","3d Brigade, 101st Airborne Division","Aviation Brigade, 101st Airborne Division","101st Airborne Division Artillery","Special Army Operations Forces","SEAL","US Rangers","Delta","Hostage Response Team (HRT)","US Marshal's Service","NEST (Nuclear Emergency Search Team)"
},

["AR"] = {
"Unit 777 (Sa'aga)","Fedayeen Saddam","Maokataha of Lebanse Army","Sultan's Special Force","101st Batallion","Maokafaha","U-Group"
},

["AU"] = {
"Special Air Service","Jagdkommando (Rangers)","Gebirgsjaegerbattalions (Mountain Warfare)","Gendarmerie Einsatz Kommando (Gek) ('COBRA')","Speziale-Einsatz Kommando (SEK)"
},

["BE"] = {
"ESR (Equipes Spécialisées de Reconnaissance)","1st. Company of Special Reconnoitre (Belgium army)","Escadron Special d'Intervention (ESI)"
},

["NL"] = { -- Dannish
"BBE (Bijzondere Bijstands Eenheid)","BSB (Brigade Speciale Beveilingsopdrachten)","Brigade Speciale Beveilinginsopdrachtn (BSB)","Jaegerkorpset","Froemanskorpset (Frogman Corps)","Sirius Patruljen (Sirius patrol)"
},

["FR"] = {
"French Foreign Legion","Groupment d'Intervention de la Gendamarie Nationale (GIGN)","Regiment Etranger des Parachutistes"
},

["SW"] = {
"Stern Unit","Vatican Guard"
},

["IT"] = {
"Groupe Interventional Speciali (part of the NOCS (Nucleo Operativo Central di Sicureza) - Lether heads)","Nucleo Operativo Centrale di Sicurezza (Central Security Operations Service)"
},

["IZ"] = { -- Israelian
"Sayaret Matka"
},

["JP"] = {
"Police Special Assault Unit"
},

["NO"] = {
"Beredskapstrop"
},

["PL"] = {
"Jednostka Wojskowa GROM"
},

["PO"] = { -- Portuguese
"Grupo de Operacoes Especialis"
},

["RZ"] = { -- Republic of Zimbabwe
"Zimbabwean Special Air Service"
},

["RU"] = {
"9th Guards Tank Division","12th Guards Tank Division","10th Guards Tank Division","47th Guards Tank Division","20th Guards Tank Division","6th Guards Tank Division","7th Guards Tank Division","11th Guards Tank Division","32nd Guards Motor Rifle Division","94th Guards Motor Rifle Division","20th Guards Motor Rifle Division","39th Guards Motor Rifle Division","57th Guards Motor Rifle Division","6th Guards Motor Rifle Division","14th Guards Motor Rifle Division","27th Guards Motor Rifle Division","25th Tank Division","9th Tank Division","207th Motor Rifle Division","19th Motor Rifle Division","Spetznaz","OMON (Special Task Force]","SMERSH","GRU","GSG-9"
},

["EC"] = { -- ECUADOR
"Unidad Antisecuestro y Extorcion (UNASE)"
},

["SA"] = {
"Battallion 911","32 Battalion","GSU (General Service Unit)","Cape Town Highlanders","Delarey Regiment","De Wet Regiment","Royal Natal Carbineers","Transvaal Horse Artillery"
},

["FI"] = {
"Nylands Brigade","Laskuvarjojaakarikoulu (Airborne Ranger School)","Osasto Karhu (Bear Unit)"
},

["SP"] = {
"Force F (Zorros)","Spanish Foreign Legion","Grupo Especial de Operaciones (GEO)","Grupose Antiterroristas Rurales (GAR)"
},

["UK"] = {
"SAS (Special Air Service)","Garda Siochana","Coldstream Guards","Grenadier Guards","Irish Guards","Scots Guards","The Argyll and Sutherland Highlanders","The Black Watch","The Devonshire and Dorset Regiment","The Duke of Wellington's Regiment","The Green Howards","The Highlanders","The Cheshire Regiment","The King's Own Royal Border Regiment","The King's Own Scottish Borderers","The King's Regiment","The Light Infantry","The Parachute Regiment","The Prince of Wales's Own Regiment of Yorkshire","The Princess of Wales's Royal Regiment","The Queen's Lancashire Regiment","The Royal Anglian Regiment","The Royal Gloucestershire, Berkshire and Wiltshire Regiment","The Royal Green Jackets","The Royal Gurkha Rifles","The Royal Highland Fusiliers","The Royal Irish Regiment","The Royal Regiment of Fusiliers","The Royal Regiment of Wales","The Royal Scots","The Royal Welch Fusiliers","The Staffordshire Regiment","The Worcestershire and Sherwood Foresters Regiment","Welsh Guards","1st The Queen's Dragoon Guards","The Royal Scots Dragoon Guards","The Royal Dragoon Guards","The Queen's Royal Hussars","9th/12th Lancers","The King's Royal Hussars","Light Dragoons","The Queen's Royal Lancers","1st Royal Tank Regiment","2nd Royal Tank Regiment"
}

};

BIO.MEDAL = {

["AM"] = {
"Medal of Honor","Distinguished Service Cross","Navy Cross","Air Force Cross","Purple Heart","Silver Star","Bronze Star","Distinguished Service Medal","Combat Infantryman badge","Southwest Asia Service Medal","Kuwait Liberation Medal","National Defence Service Medal","Armed Forces Expeditionary Medal"
},

["RU"] = {
"Order of Lenin","Hero of the Soviet Union","Hero of Socialist Labor","Red Star","Order of Glory","Medal for Combat Service","Medal for Distinguished Military Service 1st Class","Medal for Distinguished Military Service 2nd Class","Participation in the Chernobyl Disaster Cleanup","International Service in Afghanistan","Developing Oil and Gas Fields in West Siberia"
},

["AR"] = {
"Wisam al-Istihsaqaq (Order of Merit)","Wisam al-Iqdam (Order of Gallantry)","Qaladat al-Istihaqaq (Collar of Merit)","Wisam al-Wajab (Order of Duty)","Wisam al-Takrim (Order of Honour)","Grand Cross of valour (G.C.V.)","President's Medal for Shooting","Wissam 'Uum al-M'aarak (Order of the Mother of Battles)","Nuth al-Tahrir al-Kuwait (Medal for the Liberation of Kuwait)","Great Badr Chain","Wisam al-Khalifa (Order of al-Khalifa)","Wisam al-Istahaqaq al-Askari (The Order of Military Merit)","Maidalet al-Sharif (The Medal of Honour)"
},

["SA"] = {
"Castle of Good Hope Decoration","Honoris Crux","Pro Merito Decoration","Army Cross","Southern Cross Medal"
},

["NO"] = {
"Medal of Saint Olaf",
"War Cross"
},

["IT"] = {
"Ordine Militare di Savoia"
},

["IS"] = {
"Icelandic Order of the Falcon (Hin Islenzka Falkaroda)."
},

["AL"] = {
"The Skanderbeg Order"
},

["UK"] = {
"Victoria Cross","George Cross","Distinguished Service Order","Distinguished Service Cross","Military Cross"
},

["BE"] = {
"Military Cross"
},

};

BIO.PERSONALITY = {
"dependable, resourceful",
"skillful, responsive",
"courageous, yet not foolish",
"careful, yet resolute",
"open-minded, curious",
"perfectionist",
"systematic, hard-working",
"likeable, popular character",
"industrious, resourceful",
"generous, sharing",
"fearless, yet prudent",
"sharp-minded",
"straight, no-nonsense attitude",
"stable, dependable character",
"self-assured, easy-going",
"energetic, tireless worker",
"responsible, trustworthy",
"co-operative, good companion",
"performs well under stress",
"reliable under stress",
"adapts well to the situation",
"has good leadership skills",
"willing to learn",
"has a good knowledge of classical literature",
"suitable for most diverse tasks",
"has positive attitude",
"enjoys challenges",
"inspires everybody around him",
"utilises any opportunity",
"doesn't take chances",
"passionate about his work",
"has diverse interests",
"creative mind",
"charismatic personality",
"a good team player",
"a good organiser",
"strong personality",
"empathic and caring",
"personally bold, endowed with good judgement",
"capable administrator",
"swift thinker",
"productive, doesn't shy from any task",
"has good communicative skills",
"hard-driven, passionate individual",
"problem-solver",
"light hearted, courageous",
"attentive, careful",
"striking personality",
"occasionaly insubordinate",
"sometimes helpless",
"courageous, yet sometimes foolish",
"careful, sometimes coward",
"narrow-minded",
"too perfectionist",
"tends to loose the big picture",
"not popular with his mates",
"occasionaly selfish",
"too economic",
"fearless, occasionaly too ardent",
"not too sharp",
"insubordinate",
"carefree",
"ambitious",
"prefers intuitive solutions",
"individualist",
"spirited, temperamental"
};