function VISION(ELEMENT,INPUT,X,Y,SIDE,RANGE)

  if INPUT then 
    set_Property(ELEMENT,PROP_STRIKETHROUGH,false);
    OW_CUSTOM_COMMAND(1013,RANGE,SIDE-1, X, Y);
    set_Property(ELEMENT,PROP_HINT,'Click here to remove vision.');
  else
    set_Property(ELEMENT,PROP_STRIKETHROUGH,4);
    OW_CUSTOM_COMMAND(5003, X, Y, SIDE-1);
    set_Property(ELEMENT,PROP_HINT,'Click here to restore vision.');
  end
  
end