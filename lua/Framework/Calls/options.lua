local self = framework:get()

self.itemList = {}

self.itemLabels = 
{ 
	{"Give map vision",					"Allows you to give a player vision over the map. You will need to specify the range of hexagons which can be seen and click on the map. Once that's done, click on <CONFIRM> and click anywhere on the map." },
	{"Switch player side",				"Enables you to switch the side of a player. Warning: once two players share the same side, they cannot be separated again!" },
	{"Create resources",				"Allows you to create deposits anywhere on the map. Location can be customised or random. Select the amount of deposits and the maximum amount of resources per deposit." },
	{"Change unit properties",			"Dedicated interface to modify all possible properties of a unit." },
	{"Set attitudes",					"Configurates diplomatical attitudes between one side and another." },
	{"Create behaviours",				"Link special behaviours to units." },
	{"Fog of War manager",				"Two or more players can share the same fog of war if they are grouped under the same number." },
	{"Create unit",						"Design your own units!" },
	{"Create vehicle",					"Design your own vehicles!" },
	{"Create building",					"Design your own buildings!" },
	{"X!! Create oil/siberite deposit", "Used to create new sources of oil and siberite on the map." },
	{"X!! Send objectives", 			"Dedicated interface to send and modify objectives to one or several players at a time." },
	{"X!! Send queries", 				"Dedicated interface to send and modify queries to one or several players at a time." },
	{"Set technologies", 				"Defines technological advances for each side. Each technology can be set to disabled, enabled or researched. Enabling or researching any technology will unlock the requirements for it (as one technology may require other technologies researched already)."},
	{"Teleport unit/building/vehicle",  "Label"}
}

self.getItemHeight = function(id)
    return (id < 1) and 5 or getElementBottom(self.itemList[id])
end

for i, labelInfo in ipairs(self.itemLabels) do
    self.itemList[i] = getLabelEX(self.parent, anchorLT, XYWH(5, self.getItemHeight(i - 1), 50, 50), Tahoma_13, labelInfo[1], { highlight = true, font_colour = RGB(175, 175, 175), text_halign = ALIGN_LEFT, shadowtext = true, automaxwidth = 190, autosize = true, wordwrap = true, hint = labelInfo[2] })
    sgui_autosizecheck(self.itemList[i].ID)
end

self.command = function(type)
	ChangeTextureID('SGUI/Nao/hexagon',1)

	switch(CASE) : caseof
	{

	[1]	= -- "Give map vision"
	function (x)
		set_Callback(gamewindow.ID,CALLBACK_MOUSEMOVE,'Render(%x,%y);');
		set_Callback(gamewindow.ID,CALLBACK_MOUSECLICK,'MapVisionGive(%b,%x,%y);'); 
	end,

	[0]	= -- "Switch player side"
	function (x)

	end,

	}
end


function TriggerMapVision()

end;

HXS = {50, 50}
H = {}


function MapVisionGive(CASE,X,Y)
  switch(CASE) : caseof
  {
    [1]	= function (x)
	  set_Callback(gamewindow.ID,CALLBACK_MOUSECLICK,'');
	  set_Callback(gamewindow.ID,CALLBACK_MOUSEMOVE,'');
	  setVisible(MOUSEOVER,false);
	end,
    [0]	= function (x)
	  FH = OW_FINDHEX(X, Y)
	  H = OW_HEXTOSCREEN(FH.X, FH.Y)
	  if FH.FOUND == true then
		HXS = {FH.X, FH.Y};
		OW_CUSTOM_COMMAND(4448, HXS[1], HXS[2], getvalue(OWV_MYSIDE))
	  end;
	  RunCommand(101);
	end,	
  }
end