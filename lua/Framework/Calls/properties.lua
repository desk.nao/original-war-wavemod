local self = framework:get()

--local CAL = 'createOptionsWM('..(y-1)..','..(dynamic.labels[i][1]+2)..','..i..');'
	
self.constants = {
	sides 		= 	{'Neutral','Blue','Yellow','Red','Cyan','Orange','Purple','Green','Gray'},
	directions 	= 	{'Top right','Right','Bottom right','Bottom left','Left','Top left'},
	classes 	= 	{'Soldier','Engineer','Mechanic','Scientist','Sniper','Mortar','Bazooker','Desert Warrior','Apeman','Apeman Soldier','Apeman Engineer','Apeman Kamikaze'},
	skills 		=	{'0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20'},
	attributes	=	{'Stamina','Speed'},
	health		=	{'Low','Medium','Perfect'},
	colours 	=	{ RGB(175,175,175), SIDE_COLOURS[2], SIDE_COLOURS[3], SIDE_COLOURS[4], SIDE_COLOURS[5], SIDE_COLOURS[6], SIDE_COLOURS[7], SIDE_COLOURS[8], SIDE_COLOURS[9], RGB(175,175,175) },
	booleans	= 	{'Trigger event','Cancel event'},
	fuel		=	{'Empty','Low','Medium','High','Full'},
	cargo 		=	{'Add x1 crate','Add x1 oil','Add x1 siberite','Add x1 artefact'}
}

-- 1: Units, 2: Vehicles, 3: Buildings, 4: Label, 5: Callback, 6: Submenu options
self.label = {
	{true,  true,  true,  'Change side',				'', self.constants.sides 		},
	{true,  true,  true,  'Change direction', 			'', self.constants.directions	},
	{true,  false, false, 'Change unit class', 			'', self.constants.classes		},
	{true,  false, false, 'Change military skill', 		'', self.constants.skills		},
	{true,  false, false, 'Change engineering skill',	'', self.constants.skills		},
	{true,  false, false, 'Change mechanical skill', 	'', self.constants.skills		},
	{true,  false, false, 'Change scientistic skill',	'', self.constants.skills		},
	{true,  false, false, 'Change stamina', 			'', self.constants.skills		},
	{true,  false, false, 'Change speed', 				'', self.constants.skills		},
	{true,  true,  true,  'Change health',				'', self.constants.health		},
	{true,  false, false, 'Force sleep',				'', self.constants.booleans		},
	{true,  false, false, 'Force exclamation',			'', nil							},
	{false, true,  false, 'Set fuel',					'', self.constants.fuel			},
	{false, true,  false, 'Set cargo',					'', self.constants.cargo		},
	{false, false, true,  'Set resources',				'', self.constants.cargo		},
	{false, true,  false, 'Attach flag',				'', self.constants.booleans		},
	{false, false, true,  'Change building level',		'', self.constants.skills		},
	{false, false, true,  'Give name',					'', nil							},
	{false, true,  true,  'Force units to exit',		'', self.constants.booleans		},
	{false, true,  true,  'Force units to repair',		'', self.constants.booleans		},
	{false, false, true,  'Toggle animation',			'', self.constants.booleans		},
	{true,  true,  true,  'Remove unit',				'', self.constants.booleans		}
}

self.runProperties = function(type)
	local count = -1
    for _, element in ipairs(self.label) do
        if element[type] then
            count = count + 1
            getLabelEX(
                self.categories,
                anchorNone,
                XYWH(5, 2.5 + (count * 15), 171, 15),
                Tahoma_13,
                element[4],
                {
                    callback_mouseclick = 'framework.runProperty('..(count+1)..')',
                    text_halign = ALIGN_LEFT,
                    automaxwidth = 190 - 12,
                    autosize = true,
                    wordwrap = true,
                    highlight = true
                }
            )
        --sgui_setcallback(element.ID, element[5])
        end
    end
end

self.runProperties(1)

self.runProperty = function(element)
sgui_deletechildren(framework.actions.ID)
local element = self.label[element][6]
    if element ~= nil then
        for i = 1, #element do
            getLabelEX(
                self.actions,
                anchorNone,
                XYWH(5, 2.5 + ((-1+i) * 15), 171, 15),
                Tahoma_13,
                element[i],
                {
                    text_halign = ALIGN_LEFT,
                    automaxwidth = 190 - 12,
                    autosize = true,
                    wordwrap = true,
                    highlight = true
                }
            )
        end
    end
end