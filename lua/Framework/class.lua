-- CLASSES
dynamic = {};

local createInterface = function()

	local self = {}

	function self:get(element)
		return self
	end

	self.element = getElementEX(nil,anchorNone,XYWH(ScrWidth-215,100,210,LayoutHeight-275),true,{type=TYPE_SCROLLBOX, scissor=false, colour1=BLACKA(25)})

	self.parent = getElementEX(self.element,anchorNone,XYWH(0,0,210,125),true,{type=TYPE_SCROLLBOX, scissor=true, colour1=BLACKA(150)})

	--[[ Submenu 	]]	
		self.subMenu = getElementEX(self.element,anchorNone,XYWH(0,127.5,210,LayoutHeight-400),true,{type=TYPE_SCROLLBOX, colour1=BLACKA(150)})
		getLabelEX(self.subMenu,anchorNone,XYWH(5,2.5,LayoutHeight-400-17,25),Tahoma_13,'Title',{text_halign=ALIGN_MIDDLE,automaxwidth=195, autosize=true, wordwrap=true, font_colour=WHITE(), colour1=BLACKA(0), shadowtext=true, text_case = CASE_UPPER})
		getLabelEX(self.subMenu,anchorNone,XYWH(5,15,210-17,50),Tahoma_13,'Description of this menu.',{text_halign=ALIGN_MIDDLE,automaxwidth=195, autosize=true, wordwrap=true})
		self.run = getImageButtonEX(self.subMenu,anchorNone,XYWH(5,getHeight(self.subMenu)-45,210-22,25),'Run command','',"RunCommand(selected_id);",SKINTYPE_BUTTON,{font_colour_disabled=GRAY(127),})
	
	--[[ Categories ]]	
		self.categories = getElementEX(self.subMenu,anchorNone,XYWH(5,60,176,50),true,{type=TYPE_SCROLLBOX, colour1=BLACKA(150)})

	--[[ Actions 	]] 	
		self.actions = getElementEX(self.subMenu,anchorNone,XYWH(5,115,176,200),true,{type=TYPE_SCROLLBOX, colour1=BLACKA(150)})

	--[[ Scrollbars ]]
		self.subMenu.scrollbar = AddElement({type=TYPE_SCROLLBAR,parent=nil,anchor={top=true,bottom=true,left=false,right=true},visible=true,x=getX(self.element)+getWidth(self.subMenu)-12,y=getY(self.element)+getHeight(self.parent),width=12,height=getHeight(self.subMenu),colour1=Scrollbar_Colour1,colour2=Scrollbar_Colour2,texture2="scrollbar.png",})
		sgui_set(self.subMenu.ID,PROP_SCROLLBAR,self.subMenu.scrollbar.ID)

		self.parent.scrollbar = AddElement({type=TYPE_SCROLLBAR,parent=nil,anchor={top=true,bottom=true,left=false,right=true},visible=true,x=getX(self.element)+getWidth(self.parent)-12,y=getY(self.element),width=12,height=getHeight(self.parent),colour1=Scrollbar_Colour1,colour2=Scrollbar_Colour2,texture2="scrollbar.png"})
		sgui_set(self.parent.ID,PROP_SCROLLBAR,self.parent.scrollbar.ID)

		self.categories.scrollbar = AddElement({type=TYPE_SCROLLBAR,parent=nil,anchor={top=true,bottom=true,left=false,right=true},visible=true,x=getX(self.element)+getWidth(self.categories)-7,y=getY(self.element)+getY(self.subMenu)+getY(self.categories),width=12,height=getHeight(self.categories),colour1=Scrollbar_Colour1,colour2=Scrollbar_Colour2,texture2="scrollbar.png",})
		sgui_set(self.categories.ID,PROP_SCROLLBAR,self.categories.scrollbar.ID)

		self.actions.scrollbar = AddElement({type=TYPE_SCROLLBAR,parent=nil,anchor={top=true,bottom=true,left=false,right=true},visible=true,x=getX(self.element)+getWidth(self.actions)-7,y=getY(self.element)+getY(self.subMenu)+getY(self.actions),width=12,height=getHeight(self.actions),colour1=Scrollbar_Colour1,colour2=Scrollbar_Colour2,texture2="scrollbar.png",})
		sgui_set(self.actions.ID,PROP_SCROLLBAR,self.actions.scrollbar.ID)


	--[[ Logs 		]]
		self.logs = getElementEX(nil,anchorNone,XYWH(getX(self.element)+getWidth(self.element),100,210,LayoutHeight-200),false,{type=TYPE_SCROLLBOX, scissor=true, colour1=BLACKA(150)})
		getImageButtonEX(nil,anchorNone,XYWH(getX(self.element)+153,getY(self.element)+5,40,25),'Logs','','setVisible(menu.logs, not getVisible(menu.logs) ) setVisible(menu.parent, not getVisible(menu.parent) )',SKINTYPE_BUTTON,{})

	return self

end

-- CONSTANTS
framework = createInterface()

include("Framework/Calls/options")
include("Framework/Calls/properties")



--[[
local createConstants = function()

	local self = {}



	return self
end

Constants = createConstants()
local createPropertiesManager = function()
	local v = Constants
	local self = { units = {v.classes,v.skills,v.attributes,v.sides,v.health} }

	self.clear = function(ID)
		sgui_deletechildren(menu[13].ID)
		sgui_deletechildren(menu[12].ID)

		for i = 1,#self.units[ID] do 
			local callback = ''
			menu[13][i] = getLabelEX(menu[13],anchorNone,XYWH(5,2.5+((i-1)*15),171,15),Tahoma_13,self.units[ID][i][1],{text_halign=ALIGN_LEFT, callback_mouseclick=callback..' dynamic.change('..ID..','..i..',%b);', automaxwidth=190-12, autosize=true, wordwrap=true, highlight=true});
		end

	end

	return self
end]]