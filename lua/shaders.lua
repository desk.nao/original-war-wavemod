TVLINES_GLSL	  = loadGLSL('tvlines');
TVLINES_GLSL.TIME = TVLINES_GLSL:getLocation('time');

PREVIEW   = loadOGLTexture('SGUI/buildings/b-r-warehouse0000.png',true,false,false,false);
MOUSEOVER = getElementEX(nil,anchorLT,XYWH(0,0,PREVIEW.W,PREVIEW.H),false,{nomouseevent=true,subtexture=true,subcoords=SUBCOORD(0,640-PREVIEW.H,PREVIEW.W,PREVIEW.H)});

function do_Init()
	local w,h = 640,640;
	FBO = fboclass.make(w,h,true,false,0,false);
	SGUI_settextureid(MOUSEOVER.ID,FBO:getTextureID(),640,640,640,640);
	timer:single(0.0001,"SGUI_register_tick_callback('RENDER(%frametime)');");
	setSubCoords(MOUSEOVER,SUBCOORD(0,640-PREVIEW.H,PREVIEW.W,PREVIEW.H));
	setWH(MOUSEOVER,PREVIEW.W,PREVIEW.H);
end;

function ChangeTextureID(texture,ratio)
  PREVIEW = loadOGLTexture(texture..'.png',true,false,false,false);
  setVisible(MOUSEOVER,true);
  setSubCoords(MOUSEOVER,SUBCOORD(0,640-PREVIEW.H,PREVIEW.W,PREVIEW.H));
  if ratio == 0.5 then 
    setWH(MOUSEOVER,PREVIEW.W/2,PREVIEW.H/2);
  else 
    setWH(MOUSEOVER,PREVIEW.W,PREVIEW.H);
  end
  set_Callback(gamewindow.ID,CALLBACK_MOUSEMOVE,'Render(%x,%y);');
end

timer:single(1,'do_Init()');

THETIME = 0;
FRAME_PROG = 0;

function RENDER(FRAMETIME)	
	FRAME_PROG = FRAME_PROG + FRAMETIME;
	
	local needupdate = false;
	
	while FRAME_PROG >= 1/30 do				

		needupdate = true;
		THETIME = THETIME + 1/30;
	
		FRAME_PROG = FRAME_PROG - 1/30;
		if FRAME_PROG > 1/2 then
			FRAME_PROG = 0;
		end;
	end;
	
	if (not needupdate) then
		return;
	end;

	FBO:setClearColour(0,0,0,0,false);
	
	FBO:doBegin();
		OGL_BEGIN();
			OGL_GLENABLE(GL_BLEND);
			OGL_QUAD_BEGIN2D(FBO.W,FBO.H);
			
			TVLINES_GLSL:use();
			TVLINES_GLSL:setMatrixs();
			TVLINES_GLSL:setUniform1F(TVLINES_GLSL.TIME, THETIME);
									
			OGL_TEXTURE_BINDID(PREVIEW:getTextureID());
			OGL_QUAD_RENDER(0,0,PREVIEW.W,PREVIEW.H,0,0,1,1); 
			
			OGL_QUAD_END2D();
		OGL_END();
	FBO:doEnd();
end;

function Render(a, b)
FHA = OW_FINDHEX(a,b); 
	if FHA.FOUND == true then
		HA = OW_HEXTOSCREEN(FHA.X,FHA.Y); 
		AddEventSlideX(MOUSEOVER.ID,HA.X-OW_GETMVRXY(HA.X,HA.Y).X-getWidth(MOUSEOVER)/2,0.1);
		AddEventSlideY(MOUSEOVER.ID,HA.Y-OW_GETMVRXY(HA.X,HA.Y).Y-getHeight(MOUSEOVER)/2,0.1);
	end;
end;